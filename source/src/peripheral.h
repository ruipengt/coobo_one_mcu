/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __peripheral_H
#define __peripheral_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"




/****************************************************************************
*                       Timer declarations
*****************************************************************************/




/****************************************************************************
*                       LOAD CELL DECLARATION
*****************************************************************************/


typedef struct _hx711
{
	GPIO_TypeDef* gpioSck; /*Clock port*/
	GPIO_TypeDef* gpioData; /*Clock data*/
	uint16_t pinSck;
	uint16_t pinData;
	int offset;
	int gain;
        // 1: channel A, set to 1 if gain is 128
	// 2: channel B, set to 2 if gain is 32
        // 3: channel A, set to 3 if gain is 64
        float scale_divider;

} HX711;
#define SCALE_DIVIDER_GRAM      1300
extern HX711 bot_head_scale;


void HX711_Init(HX711 data);
HX711 HX711_Tare(HX711 data, uint8_t times);
int HX711_Value(HX711 data);
int HX711_AverageValue(HX711 data, uint8_t times);
void loadcells_init(void);
float get_weight(HX711 data, uint8_t times);
int get_tared_value(HX711 data, uint8_t times);


#ifdef __cplusplus
}
#endif
#endif 