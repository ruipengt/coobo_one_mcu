#include "global.h"

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/*External handles*/
extern SPI_HandleTypeDef bot_head_spi_handle;

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}





void PendSV_Handler(void)
{
}

void BOTHEAD_SPI_DMA_TX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(bot_head_spi_handle.hdmatx);
}

void BOTHEAD_SPI_DMA_RX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(bot_head_spi_handle.hdmarx);
}

void SPI4_IRQHandler(void)
{
   HAL_SPI_IRQHandler(&drawer_module_spi_handle);
}

void DMA2_Stream0_IRQHandler(void)
{
  HAL_DMA_IRQHandler(&hdma_adc1);
}

void DMA2_Stream4_IRQHandler(void)
{
  HAL_DMA_IRQHandler(drawer_module_spi_handle.hdmatx);
}

void BOTHEAD_SPI_IRQHandler(void)
{
  HAL_SPI_IRQHandler(&bot_head_spi_handle);
}

void USART1_IRQHandler(void)
{
  HAL_UART_IRQHandler(&huart1);
}

void DMA2_Stream7_IRQHandler(void)
{
  HAL_DMA_IRQHandler(&hdma_usart1_tx);
}

void DMA2_Stream2_IRQHandler(void)
{
  HAL_DMA_IRQHandler(&hdma_usart1_rx);
}

void DMA2_Stream1_IRQHandler(void)
{
    HAL_DMA_IRQHandler(&hdma_usart6_rx);
}

void DMA2_Stream6_IRQHandler(void)
{
    HAL_DMA_IRQHandler(huart6.hdmatx);
}
void USART6_IRQHandler(void)
{
    HAL_UART_IRQHandler(&huart6);
}

extern uint32_t display_counter;
extern uint32_t pwm_period;
void TIM2_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim2);
//          display_counter++;
//
//        if(HAL_SPI_Transmit_DMA(&bot_head_spi_handle, (uint8_t *)&display_counter, 1) != HAL_OK)
//        {
//            Error_Handler();
//        }
//            pwm_period += 10;
//    if (pwm_period >= 100)
//      pwm_period = 0;
//    else
//      htim4.Instance->CCR1 = pwm_period;
}


/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
  HAL_IncTick();
  HAL_SYSTICK_IRQHandler();
  QF_TICK_X(0U, (void *)0);
}


void EXTI3_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}
/****************************************************************************
*                       Call backs
*****************************************************************************/
//
//void HAL_GPIO_EXTI_Callback(GPIO_Pin)
//{
//}

