#ifndef global_h
#define global_h

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "stm32f401xc.h"
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "bsp.h"
#include "gpio.h"
#include "qpc.h"
#include "qf.h"
#include "ao_declare.h"
#include "ao_signals.h"
#include "peripheral.h"
#include "spi.h"
#include "usart.h"
#include "global_def.h"
#include "bot_controller.h"
#include <string.h>
#include "timer.h"
#include "pin_def.h"
#include "adc.h"

#endif