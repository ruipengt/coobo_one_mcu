#ifndef global_def_h
#define global_def_h

#include "qpc.h"

/****************************************************************************
*                      defines
*****************************************************************************/

#define         ON      1
#define         OFF     0

#define			OPEN	0 /*Servo motor open*/
#define			CLOSED	1 /*Servo motor close*/
enum motor_state
{
    STOP,
    CW,
    CCW
};

enum ProcessState {
    PROCESSING,
    FINISHED
};

enum KernalUnawareISRs
{
    MAX_KERNEL_UNAWARE_CMSIS_PRI  
};
Q_ASSERT_COMPILE(MAX_KERNEL_UNAWARE_CMSIS_PRI <= QF_AWARE_ISR_CMSIS_PRI);

enum KernalAwareISRs
{

    SYSTICK_PRIO = QF_AWARE_ISR_CMSIS_PRI,
    BOTHEAD_DMA_PRIO,
    BOTHEAD_SPI_PRIO,
    DRAWER_MODULE_DMA_PRIO,
    DRAWER_MODULE_SPI_PRIO,
    BOTHEAD_TIM_PRIO,

    GRBL_USART_PRIO,
    GRBL_USART_DMA_RX_PRIO,
    GRBL_USART_DMA_TX_PRIO,
    TABLET_USART_PRIO,
    TABLET_USART_DMA_PRIO,
    DRAWER_PRIO,
    MAX_KERNEL_AWARE_CMSIS_PRI
};
Q_ASSERT_COMPILE(MAX_KERNEL_AWARE_CMSIS_PRI <= (0xFF >>(8-__NVIC_PRIO_BITS)));

void Error_Handler(void);

/* Size of buffer */
#define BUFFERSIZE                       (COUNTOF(aTxBuffer) - 1)

/* Exported macro ------------------------------------------------------------*/
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))
/* Exported functions ------------------------------------------------------- */

#endif