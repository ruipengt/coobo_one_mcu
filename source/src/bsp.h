#ifndef BSP_H
#define BSP_H
#include "qpc.h"

void bsp_init(void);
void send_event(uint8_t sig, QMActive * target);
uint32_t read_pin(uint32_t pin);
void write_pin(uint32_t pin, uint32_t state);
void recurring_task_start(uint32_t task_id, uint32_t task_info_1, uint32_t task_info_2);
void SendEvent(uint8_t sig, QActive * target);
void grbl_usart_start(void);
void tablet_usart_start(void);
uint32_t stall_detect(void);

#endif