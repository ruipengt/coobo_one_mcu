#ifndef PROTOCOL_MANAGER_H
#define PROTOCOL_MANAGER_H

#include <stdint.h>
/*
* Information needed to control bot head movement
*/

/*bot_reference used to drive all other coordinate. so that all 
coordinates are not directly depend on absolute home (0,0,0)
The position of this reference y,z is at center of bottom inside
cartridge, and Z is the second cabinet center from the left*/
#define         BOT_REF_Y                               0
#define         BOT_REF_X                               0
#define         BOT_REF_Z                               0

#define         CABINET_PARKING_1_X                         0
#define         CABINET_PARKING_2_X                         81.5f
#define         CABINET_PARKING_3_X                         163.5f
#define         CABINET_PARKING_4_X                         343.5f
#define         CABINET_PARKING_5_X                         322.5f
#define         CABINET_HOOK_CLEARENCE                      5.0f

#define         BOT_HEAD_CENTER_TO_CARIDGE_HANDLE           46.95f
#define         CARIDGE_HANDLE_TO_CABINET_CENTER            37.5f
#define         CABINET_ACCESS_CLEARANCE                    20.0f
#define         BOT_HEAD_POSITION_LEFT                      1
#define         BOT_HEAD_POSITION_RIGHT                     2
#define         CABINET_OPENING_OFFSET_X                    80.0f
#define         CARTRIDGE_SPACEING_Y                        90.5f
#define         CARTRIDGE_SPACEING_Z                        178.0f
#define         PARKING_PLANE_Z_TO_BOT_REF_Z                255.0f
#define         CABINET_SPACING                             80.0f
#define         CABINET_ENGAGE_Z_TO_BOT_REF_Z               242.0f
#define         BOT_HEAD_LEFT                               1
#define         BOT_HEAD_RIGHT                              -1
#define         DRAWER_CENTER_X_TO_REF_X                    167.5f
#define         DRAWER_CENTER_Y_TO_REF_y                    152.82f   

#define         POT_DISPENSE_Y_TO_DRAWER_CENTER_Y           20.0f
#define         POT_DISPENSE_Z_TO_BOT_REF_Z                 350.0f

/*
* Macro to extract info from id
*/
#define     CABINET_NUM(A)  (A >> 8) & 0xF
#define     ROW_NUM(A)      (A >> 4) & 0xF
#define     COLUMN_NUM(A)    A & 0xF


typedef struct
{
    uint32_t bot_head_clamp_state;
    uint32_t a_axis_state;
    uint32_t cabinet1_location;
    uint32_t cabinet2_location;
    uint32_t cabinet3_location;
    uint32_t cabinet4_location;
    float x;
    float y;
    float z;
    float b_axis_state;
} bot_state_info_t;


typedef struct
{
    uint32_t cartridge_id;
	uint32_t cartridge_type;
	uint32_t cartridge_cabinet_num;
	uint32_t cartridge_row_num;
	uint32_t cartridge_column_num;
    uint32_t bot_head_orientation;
	/* cabinet parameters */
	float cabinet_close_x; /*position to open cabinet*/
    float opened_cabinet_center_x;
    float closed_cabinet_center_x;
	float closed_cabinet_hook_x;
    float opened_cabinet_hook_x;
	float cabinet_access_x;
    float cabinet_retract_x;
    float cabinet_engage_z;
	
	/* cartridge position parameters */
    float cartridge_engage_x;
	float cartridge_y;
	float cartridge_z;
    float cartridge_liftup_z;
    float parking_plane_z;
    
} cartridge_info_t;

typedef struct
{
    uint32_t pot_id;
    uint32_t rotation_direction;
    uint32_t rotation_angle;
    float drawer_center_x;
    float drawer_center_y;
    /* pot parameters */
	float pot_dispense_x;
	float pot_dispense_y;
	float pot_dispense_z;
    
} pot_position_info_t;



typedef struct
{
    uint32_t scale_tare_flag;
    uint32_t scale_measure_flag;
    uint32_t scale_measurement_counter;
    float weight_to_dispense;
    float current_raw_weight;
    float bot_head_weight;
    float cartridge_weight;
    float bare_weight_before_dispensing;
    float current_bare_weight;
    float weight_dispensed;
} bot_head_scale_info_t;

/*
* These are structures to store information needed for processing
* recipe commands, these will be filled first then the recipe command
* will be excecuted
*/

typedef struct
{
    uint32_t recipe_cmd_id;
    cartridge_info_t catridge_info;
    pot_position_info_t pot_info;
    bot_head_scale_info_t scale_info;
} cartridge_dispense_cmd_info_t;

typedef struct
{
    uint32_t recipe_cmd_id;
    uint32_t dispense_duration;
    pot_position_info_t pot_pos_info;
} water_dispense_cmd_info_t;

typedef struct
{
    uint32_t pot_id;
    uint32_t pot_stir_direction;
    uint32_t pot_stir_timeout;
} pot_stir_cmd_info_t;


typedef struct
{
    cartridge_dispense_cmd_info_t cdci;
    water_dispense_cmd_info_t wdci;
    pot_stir_cmd_info_t psci;
} recipe_cmd_support_info_t;

enum pot_id
{
//    INVALID_POT_ID = 0,
    TOP_LEFT_POT,
    TOP_RIGHT_POT,
    BOTTOM_LEFT_POT,
    BOTTOM_RIGHT_POT
};

enum recipe_cmd_id
{
    R_CMD_ID_INVALID = 0,
    R_EJECTION_CARTRIDGE_DISPENSE, //1
    R_CONDIMENT_CARTRIDGE_DISPENSE, //2
    R_ROTATIONAL_CARTRIDGE_DISPENSE, //3
    R_WATER_DISPENSE, //4
    R_POT_STIR, //5
    R_NUM_OF_RECIPE_CMD_MAX
};

enum cabinet_id
{
    INVALID_CAB = 0,
    CABINET_1,
    CABINET_2,
    CABINET_3,
    CABINET_4
};

enum cartridge_type
{
    INVALID_CARTRIDGE_TYPE = 0,
    CONDIMENT_TYPE,
    EJECTION_TYPE,
    ROTATIONAL_TYPE,
    DUMPING_TYPE,
    CARTRIDGE_TYPE_NUM_MAX
};

recipe_cmd_support_info_t * recipe_cmd_support_info_getter (void);
void prepare_recipe_cmd_support_info(void);
void set_cartridge_dispense_cmd_info(void);
void set_pot_position_info(pot_position_info_t * pi, uint32_t pot_id);
void set_cartridge_info(cartridge_info_t * ci, uint32_t cartridge_id);
void rc_array_init(void);
void set_cartridge_info(cartridge_info_t * ci, uint32_t cartridge_id);
uint32_t recipe_cmd_id_getter(void);
void next_recipe_cmd(void);
void reset_cmd_info(void);
#endif