#ifndef __spi_H
#define __spi_H

#include "stm32f4xx_hal.h"
/****************************************************************************
*                       SPI declarations
*****************************************************************************/
/* Definition for BOTHEAD_SPI clock resources */
#define BOTHEAD_SPI                             SPI1
#define BOTHEAD_SPI_CLK_ENABLE()                __HAL_RCC_SPI1_CLK_ENABLE()
#define DMAx_CLK_ENABLE()                       __HAL_RCC_DMA2_CLK_ENABLE()
#define BOTHEAD_SPI_SCK_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOA_CLK_ENABLE()
#define BOTHEAD_SPI_MISO_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE() 
#define BOTHEAD_SPI_MOSI_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE() 

#define BOTHEAD_SPI_FORCE_RESET()               __HAL_RCC_SPI1_FORCE_RESET()
#define BOTHEAD_SPI_RELEASE_RESET()             __HAL_RCC_SPI1_RELEASE_RESET()

/* Definition for BOTHEAD_SPI Pins */
#define BOTHEAD_SPI_SCK_PIN                     GPIO_PIN_5
#define BOTHEAD_SPI_MOSI_PIN                    GPIO_PIN_7
#define BOTHEAD_SPI_AF                          GPIO_AF5_SPI1
#define BOTHEAD_SPI_PORT                        GPIOA


/* Definition for BOTHEAD_SPI's DMA */
#define BOTHEAD_SPI_TX_DMA_CHANNEL              DMA_CHANNEL_3
#define BOTHEAD_SPI_TX_DMA_STREAM               DMA2_Stream3

/* Definition for BOTHEAD_SPI's NVIC */
#define BOTHEAD_SPI_IRQn                        SPI1_IRQn
#define BOTHEAD_SPI_IRQHandler                  SPI1_IRQHandler
#define BOTHEAD_SPI_DMA_TX_IRQn                 DMA2_Stream3_IRQn
#define BOTHEAD_SPI_DMA_TX_IRQHandler           DMA2_Stream3_IRQHandler
   
extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
extern SPI_HandleTypeDef hspi3;
extern SPI_HandleTypeDef drawer_module_spi_handle;
extern SPI_HandleTypeDef bot_head_spi_handle;
extern void Error_Handler(void);
void bot_head_spi_init(void);
void drawer_module_spi_init(void);



#endif