/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __adc_H
#define __adc_H

extern ADC_HandleTypeDef hadc1;
extern DMA_HandleTypeDef hdma_adc1;
extern void Error_Handler(void);
void MX_ADC1_Init(void);

#endif /*__ adc_H */
