#ifndef bot_control_h
#define bot_control_h

#include <stdint.h>
#include "protocol_manager.h"

#define		CMD_ID_MIN		    1
#define		CMD_SEQ_ID_MIN	    1
#define		CMD_SEQ_BUF_SIZE	20

#define     FEED_RATE_XY_NO_LOAD     5000U
#define     FEED_RATE_XY_LOADED      3500U
#define     FEED_RATE_Z              1850U

#define     CLEAR_BUF_BIT(A,B)   (A) &= (~(1UL << (B)))
#define     SET_BUF_BIT(A,B)     (A) |= (1UL << (B)) 
#define     POT_BASE(A)      4 * (3 - A) + 16 /*base offset bit for each pot*/

#define     DISPENSING_ATTEMPT_MAX   100U
#define     BOT_HEAD_WEIGHT          500.0f

#define     DISPENSE_MOTOR_ROTATION_ANGLE_LIMIT      500

//#define		CABINET_SPACING						60.00f
//#define		BIG_CARTRIDGE_COLUMN_SPACING		170.00f
//#define		BIG_CARTRIDGE_ROW_SPACING			80.00f
//#define		SMALL_CARTRIDGE_COLUMN_SPACING		40.00f
//#define 	PARKING_PLANE_Z_AXIS				50.00f
//#define		ENGAGE_CABINET_SPOT					22.00f
//#define		BOT_CENTER_X						50.00f
//#define		BOT_CENTER_Y						50.00f

/*The references*/
/*
* Bot commands start with B is designed to do simple movement, these commands
* are called by sequence commands (start with S) commands, which represent a series of 
* B movements, one can also see it as a snippet of B movement.These S commdands
* then will be called by R (recipe) commands. which represent one single command
* sent from tablet. 
* In summary. R_command -> S_command -> B_commands
*/

enum bot_head_shift_register_io
{
    CLAMP_SERVO_BIT = 1,
    DISPENSING_MOTOR_2_BIT,
    DISPENSING_MOTOR_1_BIT,
    A_AXIS_STEPPER_4_BIT,
    A_AXIS_STEPPER_3_BIT,
    A_AXIS_STEPPER_2_BIT,
    A_AXIS_STEPPER_1_BIT
};

enum bot_cmd_id
{
    B_INVALID_ID = 0,
    /* grbl gcode type command */
	B_GCODE_GO_TO_PARKING_PLANE = CMD_ID_MIN,
	B_GCODE_GO_TO_CABINET_LATCH,
	B_GCODE_CABINET_ENGAGE,
	B_GCODE_GO_TO_CABINET_ACCESS_SPOT,
	B_GCODE_GO_TO_CARTRIDGE_FRONT_SPOT,
    B_GCODE_GO_TO_CARTRIDGE_FRONT_SPOT_LOADED,
	B_GCODE_CARTRIDGE_ENGAGE,
    B_GCODE_LOWER_CARTRIDGE_IN_PLACE,
    B_GCODE_CARTRIDGE_LIFTUP,
	B_GCODE_GO_TO_RETRACTION_SPOT_X,
	B_GCODE_GO_TO_PARKING_PLANE_CENTER,
	B_GCODE_CHANGE_TO_ABSOLUTE_MODE,
	B_GCODE_CHANGE_TO_RELATIVE_MODE,
	B_GCODE_GO_TO_CARTRIDGE,
	B_GCODE_RETURN_FROM_CARTRIDGE,
	B_GCODE_GO_TO_POT_DISPENSING_SPOT,
    B_GCODE_GO_TO_CABINET_OPEN_SPOT,
    B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
	B_GCODE_BOT_HEAD_MOVEMENT_CHECK,
    B_LOWER_TO_DISPENSE_Z,
	B_GCODE_ID_MAX,
    
    /*Scale measure command*/
    B_SCALE_MEASURE,
    
    B_BOT_HEAD_TILT_RESET,
    B_BOT_HEAD_ORIENT,
    /*dispensing type command*/
    B_EJECTING_DISPENSE_START,
    B_ROTATION_DISPENSE_START,
    B_DUMPING_DISPENSE_START,
    
	/* other control */

	B_CLOSE_CLAMP,
	B_OPEN_CLAMP,
    B_OPEN_LID,
    B_CLOSE_LID,
    B_POT_STIR,
    B_POT_STIR_TEST,
    B_SCALE_TARE,
    B_BOT_HEAD_TILT,
    B_EJECTION_DISPENSE,
    B_SHAKE_DOWN,
    B_SHAKE_UP,
    B_DISPENSING_MOTOR_RESET,
    B_DISPENSING_MOTOR_RESET_90,
    B_ROTATION_DISPENSE,
    B_FULLY_DUMP,
    B_RE_DUMP,
    B_DISPENSE_SET_BACK,
    B_LIFT_UP_FOR_RE_DUMPING,
    B_RESET_A_AXIS,
    B_A_AXIS_FRONT,
    B_A_AXIS_RIGHT,
    B_HOMING,
    B_BOT_HEAD_UPSIDE_DOWN,
	B_NUM_OF_BOT_ID_MAX
};

enum sequence_id
{
    S_INVALID_ID = 0,
    S_CALIBRATION_UPRIGHT,
    S_CALIBRATION_UPSIDE_DOWN,
	S_PICKUP_UPRIGHT_CARTRIDGE_FROM_FRIDGE,
    S_PICKUP_UPSIDE_DOWN_CARTRIDGE_FROM_FRIDGE,
    S_RETURN_UPRIGHT_CARTRIDGE_TO_FRIDGE,
    S_RETURN_UPSIDE_DOWN_CARTRIDGE_TO_FRIDGE,
    
    S_DISPENSE_FROM_CARTRIDGE,
    S_RETURN_CARTRIDGE_NORMAL,
    S_RETURN_CARTRIDGE_UPSIDE_DOWN,
    S_PREPARE_FOR_DISPENSING,
    S_EJECTING_DISPENSE,
    S_ROTATION_DISPENSE,
    S_DUMPING_DISPENSE,
    
	S_NUM_OF_SEQ_ID_MAX
};

enum cmd_type
{
    EMPTY_CMD_TYPE,
	GCODE_TYPE,
    DISPENSE_TYPE,
    BOT_HEAD_SCALE_MEASURE_TYPE,
	STANDARD_TYPE
};

enum run_cmd_seq_info
{
	GCODE_SENT_OK,
	GCODE_SENT_ERROR,
	GCODE_EXCECUTE_COMPLETE,
	GCODE_STILL_RUNNING
};
enum drawer_module_shift_register_id
{
    STIR_MOTOR_N,
    STIR_MOTOR_P,
    LIFT_MOTOR_N,
    LIFT_MOTOR_P
};


enum lift_motor_direction
{
    MOTOR_DISABLE,
    DOWN,
    UP
};

enum time_out
{
    INFINITE = 0,
    LID_ENGAGE_T = 500
};


typedef struct 
{
    uint32_t lift_motor_state;
    uint32_t lift_motor_direction;
    uint32_t lift_motor_pwm_period;
    uint32_t lift_motor_pwm_duty_cycle;
    uint32_t lift_motor_moving_timeout;
    uint32_t lift_motor_pwm_counter;

    uint32_t stir_motor_direction;
    uint32_t stir_motor_pwm_period;
    uint32_t stir_motor_infinite_rotation;
    uint32_t stir_motor_timeout;
    uint32_t stir_motor_pwm_duty_cycle;
    uint32_t stir_motor_pwm_counter;
} b_pot_control_info_t;

void timer_call_back(void);
void toggle_clamp(void);
uint32_t current_cmd_type(void);
void process_current_cmd(void);
void cmd_processing_done(void);
void calculate_cmd_info(void);
void bot_control(uint32_t cmd);
void next_cmd(int32_t cmd_step);
void bot_head_update(void);
void a_axis_motor_control(uint32_t direction, uint32_t angle);
void b_axis_motor_control(uint32_t angle);
void dispensing_motor_control(uint32_t direction, uint32_t angle);
void construct_r_cmd_seq(uint32_t current_r_cmd, uint32_t * recipe_cmd_seq);
void start_excecuting_recipe_cmd(void);
void tare_load_cell(void);
void update_drawer_shift_register(void);
void pot_lift_motor_control(uint32_t pot_id, uint32_t direction);
void pot_stir_motor_control(uint32_t pot_id, uint32_t direction, uint32_t timeout);
uint32_t current_cmd_id(void);
void dispensing_set_back(uint32_t dispensing_method);
void bot_head_orient(uint32_t direction, uint32_t angle);

#endif