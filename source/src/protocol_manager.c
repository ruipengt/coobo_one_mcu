#include "protocol_manager.h"
#include "global.h"

#define RECIPE_STR_BUF_LEN  30
/*
*   Recipe command excecution flow
*   -> tablet send one recipe command
*   -> usart receiver detect endline character and send RECIPE_CMD_EXCUTE_START_SIG to ao_protocol_manager
*   -> recipe_cmd_decode() set recipe_cmd_id 
*   -> fill out recipe command execution support info by calling various set_xxx_info()
*   -> send event to ao_bot_controller to start executing the recipe command
*   -> recipe command started by assemble bot command together to array
*   -> after assembly, bot_controller state machine then execute bot commands one by one
*   -> after command sequence finished, finishing event is sent to protocol manager
*   -> protocol manager report to tablet with recipe cmd finish successfully or not.
*   -> start over.
*/

recipe_cmd_support_info_t rc_info = {0};
static uint32_t recipe_cmd_counter = 0;
static uint32_t recipe_cmd_all_finished_flag = 0;
static const recipe_cmd_support_info_t empty_rc_info; /*Used for resetting the support info*/
/*cmd id, cartridge id, cartridge type, pot id, dispensing method, dipsensing quantity*/
/*
example
    {
        "1 "      --cmd id
        "0x2222 " --cartridge id
        "3 "      --cartridge type
        "0 "      --pot id
        "21.5";   --weight to dispense
    }
*/
/*This dummy sequence is for test purpose, in production this will be replaced by actual tablet*/
static uint8_t recipe_dummy_cmds[10][RECIPE_STR_BUF_LEN] = 
{
    {
        "1 "      /*R_EJECTION_CARTRIDGE_DISPENSE*/
        "0x212 " /*cartridge id*/
        "2 "      /*cartridge type*/
        "0 "      /*pot id*/
        "71.5"   /*weight to dispense*/
    },
    {
        "2 "    /*R_CONDIMENT_CARTRIDGE_DISPENSE*/
        "0x213 "
        "1 "
        "0 "
        "50.1" 
    }
};
static uint8_t recipe_cmd_str[RECIPE_STR_BUF_LEN] = "";
static uint32_t recipe_cmd_id = 0;


const float cartridge_weight[CARTRIDGE_TYPE_NUM_MAX] =
{
    [CONDIMENT_TYPE] = 56.0f,
    [EJECTION_TYPE] = 70.0f,
    [ROTATIONAL_TYPE] = 68.7
};

void prepare_recipe_cmd_support_info(void)
{
    sscanf((char *) recipe_cmd_str, "%d", &recipe_cmd_id);
    switch(recipe_cmd_id)
    {
        case R_EJECTION_CARTRIDGE_DISPENSE:
        case R_CONDIMENT_CARTRIDGE_DISPENSE:
        case R_ROTATIONAL_CARTRIDGE_DISPENSE:
            set_cartridge_dispense_cmd_info();
        break;
    }
}

recipe_cmd_support_info_t * recipe_cmd_support_info_getter (void)
{
    return &rc_info;
}
uint32_t recipe_cmd_id_getter(void)
{
    return recipe_cmd_id;
}
/*
* Functions to decode command for dispensing certain amount from cartridges
*/

void set_cartridge_dispense_cmd_info(void)
{   
    uint32_t cartridge_id;
    uint32_t pot_id;
    uint32_t dispensing_method;
    uint32_t cartridge_type;
    float weight_to_dispense;
    sscanf((char* )recipe_cmd_str, "%d %x %d %d %f", &recipe_cmd_id, &cartridge_id, &cartridge_type, &pot_id,
           &weight_to_dispense);
    rc_info.cdci.recipe_cmd_id = recipe_cmd_id;
    rc_info.cdci.scale_info.cartridge_weight = cartridge_weight[cartridge_type];
    rc_info.cdci.scale_info.weight_to_dispense = weight_to_dispense;
    rc_info.cdci.catridge_info.cartridge_type = cartridge_type;
    
    set_cartridge_info(&(rc_info.cdci.catridge_info), cartridge_id);
    set_pot_position_info(&(rc_info.cdci.pot_info), pot_id);
}

void set_pot_position_info(pot_position_info_t * pi, uint32_t pot_id)
{
    pi->pot_id = pot_id;
    pi->drawer_center_x = DRAWER_CENTER_X_TO_REF_X - BOT_REF_X;
    pi->drawer_center_y = DRAWER_CENTER_Y_TO_REF_y - BOT_REF_Y;
    pi->pot_dispense_z = BOT_REF_Z - POT_DISPENSE_Z_TO_BOT_REF_Z;
    switch(pot_id)
    {
        case TOP_LEFT_POT:
            pi->rotation_angle = 95;
            pi->rotation_direction = CCW;
            pi->pot_dispense_x = pi->drawer_center_x - 50;
            pi->pot_dispense_y = pi->drawer_center_y + 50;
        break;
        case TOP_RIGHT_POT:
            pi->rotation_angle = 270;
            pi->rotation_direction = CCW;
            pi->pot_dispense_x = pi->drawer_center_x;
            pi->pot_dispense_y = pi->drawer_center_y;
        break;
        case BOTTOM_LEFT_POT:
            pi->rotation_angle = 90;
            pi->rotation_direction = CCW;
            pi->pot_dispense_x = pi->drawer_center_x;
            pi->pot_dispense_y = pi->drawer_center_y;
        break;
        case BOTTOM_RIGHT_POT:
            pi->rotation_angle = 180;
            pi->rotation_direction = CCW;
            pi->pot_dispense_x = pi->drawer_center_x;
            pi->pot_dispense_y = pi->drawer_center_y;
        break;
        default:
            break;
    }
}

void set_cartridge_info(cartridge_info_t * ci, uint32_t cartridge_id)
{
    ci->cartridge_id = cartridge_id;
    //ci->cartridge_type = CARTRIDGE_TYPE(cartridge_id);
    ci->cartridge_cabinet_num = CABINET_NUM(cartridge_id);
    ci->cartridge_row_num = ROW_NUM(cartridge_id);
    ci->cartridge_column_num = COLUMN_NUM(cartridge_id);
    ci->cabinet_engage_z = BOT_REF_Z - CABINET_ENGAGE_Z_TO_BOT_REF_Z;
    ci->parking_plane_z = BOT_REF_Z - PARKING_PLANE_Z_TO_BOT_REF_Z;
    
    ci->cartridge_y = BOT_REF_Y + CARTRIDGE_SPACEING_Y * (ci->cartridge_column_num - 1);
    ci->cartridge_z = BOT_REF_Z + CARTRIDGE_SPACEING_Z * (ci->cartridge_row_num - 1);
                        //+ 0.7 * (ci->cartridge_column_num - 1); /*for y axis tilting compensation*/
    ci->cartridge_liftup_z = ci->cartridge_z + 8;
    switch (ci->cartridge_cabinet_num)
    {
        case CABINET_2:
            ci->closed_cabinet_center_x = CABINET_PARKING_3_X;
            ci->opened_cabinet_center_x = CABINET_PARKING_1_X;
            ci->closed_cabinet_hook_x = ci->closed_cabinet_center_x + CABINET_HOOK_CLEARENCE;
            ci->opened_cabinet_hook_x = ci->opened_cabinet_center_x - CABINET_HOOK_CLEARENCE;
            ci->cartridge_engage_x = ci->opened_cabinet_center_x + BOT_HEAD_CENTER_TO_CARIDGE_HANDLE 
                                             + CARIDGE_HANDLE_TO_CABINET_CENTER - 3;
            ci->cabinet_access_x = ci->cartridge_engage_x + CABINET_ACCESS_CLEARANCE;
            ci->cabinet_retract_x = CABINET_PARKING_3_X;
            ci->bot_head_orientation = BOT_HEAD_LEFT;
            break;
        case CABINET_3:
            ci->closed_cabinet_center_x = CABINET_PARKING_4_X;
            ci->opened_cabinet_center_x = CABINET_PARKING_2_X;
            ci->closed_cabinet_hook_x = ci->closed_cabinet_center_x + CABINET_HOOK_CLEARENCE;
            ci->opened_cabinet_hook_x = ci->opened_cabinet_center_x - CABINET_HOOK_CLEARENCE;
            ci->cartridge_engage_x = ci->opened_cabinet_center_x + BOT_HEAD_CENTER_TO_CARIDGE_HANDLE 
                                             + CARIDGE_HANDLE_TO_CABINET_CENTER - 3;
            ci->cabinet_access_x = ci->cartridge_engage_x + CABINET_ACCESS_CLEARANCE;
            ci->cabinet_retract_x = CABINET_PARKING_4_X;
            ci->bot_head_orientation = BOT_HEAD_LEFT;
            break;
        case CABINET_4:
            ci->closed_cabinet_center_x = CABINET_PARKING_5_X;
            ci->opened_cabinet_center_x = CABINET_PARKING_3_X;
            ci->closed_cabinet_hook_x = ci->closed_cabinet_center_x + CABINET_HOOK_CLEARENCE;
            ci->opened_cabinet_hook_x = ci->opened_cabinet_center_x - CABINET_HOOK_CLEARENCE;
            ci->cartridge_engage_x = ci->opened_cabinet_center_x
                                     - BOT_HEAD_CENTER_TO_CARIDGE_HANDLE
                                     - CARIDGE_HANDLE_TO_CABINET_CENTER + 3;
            ci->cabinet_access_x = ci->cartridge_engage_x - CABINET_ACCESS_CLEARANCE;
            ci->cabinet_retract_x = CABINET_PARKING_3_X;
            ci->bot_head_orientation = BOT_HEAD_RIGHT;
            break;
        default:
            while(1);
    }
}

void next_recipe_cmd(void)
{
    if(recipe_dummy_cmds[recipe_cmd_counter][0] != '\0')
    {
        strcpy(recipe_cmd_str, (char *) &recipe_dummy_cmds[recipe_cmd_counter]);
        
        prepare_recipe_cmd_support_info();
        start_excecuting_recipe_cmd();
        recipe_cmd_counter++;
    }
    else
    {
        send_event(RECIPE_CMD_SEQ_EMPTY_SIG, ao_protocol_manager);
        recipe_cmd_all_finished_flag = 1;
        recipe_cmd_counter = 0;
    }
}

void reset_cmd_info(void)
{
    rc_info = empty_rc_info;
    //memset(&rc_array[recipe_cmd_id], 0, sizeof(recipe_cmd_support_info_t));
}