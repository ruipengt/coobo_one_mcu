#include "global.h"
#include "peripheral.h"

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

int main()
{
    HAL_Init();
    SystemClock_Config();
	
    gpio_init();
    //interrupt_gpio_init();
    bot_head_spi_init();
    drawer_module_spi_init();
    //motor_control_pwm_start();
    loadcells_init();
    base_timer_start();
    bot_head_b_axis_servo_pwm_start();
    bot_head_clamp_servo_pwm_start();
    MX_USART1_UART_Init();
    grbl_usart_start();
    MX_USART6_UART_Init();
    tablet_usart_start();
    a_axis_motor_control(CW, 360);
    MX_ADC1_Init();
    drawer_lift_motor_stall_detect_adc_start();
                         
    static QEvt const * botControllerQueueSto[50];
    static QEvt const * asyncTaskQueueSto[50];
    static QEvt const * protocolMgrQueueSto[50];

    static QF_MPOOL_EL(QEvt)    smlPoolSto[50];

    
    static QSubscrList  subscrSto[30];
    bot_controller_ctor();
    async_task_processor_ctor();
    protocol_manager_ctor();
    
    QF_init();
    QF_psInit(subscrSto, Q_DIM(subscrSto));
    
    QF_poolInit(smlPoolSto, sizeof(smlPoolSto), sizeof(smlPoolSto[0]));

    
        /* start the active objects... */
    QACTIVE_START(ao_bot_controller,
                  3U,                /* QP priority */
                  botControllerQueueSto,  Q_DIM(botControllerQueueSto), /* evt queue */
                  (void *)0, 0U,     /* no per-thread stack */
                  (QEvt *)0);        /* no initialization event */
            /* start the active objects... */
    QACTIVE_START(ao_async_task_processor,
                  2U,                /* QP priority */
                  asyncTaskQueueSto,  Q_DIM(asyncTaskQueueSto), /* evt queue */
                  (void *)0, 0U,     /* no per-thread stack */
                  (QEvt *)0);        /* no initialization event */
    QACTIVE_START(ao_protocol_manager,
                  1U,                /* QP priority */
                  protocolMgrQueueSto,  Q_DIM(protocolMgrQueueSto), /* evt queue */
                  (void *)0, 0U,     /* no per-thread stack */
                  (QEvt *)0);        /* no initialization event */
    return QF_run();
}


/** System Clock Configuration
* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!Important!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
* Make sure to update HSE_VALUE stm32f4xx.h, when clock is changed.  
* System clock is set to 84000000Hz.
*/

void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

    /**Enables the Clock Security System 
    */
  HAL_RCC_EnableCSS();

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, SYSTICK_PRIO, 0);
}


/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}


  


