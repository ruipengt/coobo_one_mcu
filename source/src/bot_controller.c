#include "bot_controller.h"
#include "global.h"
#include <string.h>
#include "protocol_manager.h"

void cmd_execute_delay(uint32_t delay_ms);

static uint32_t cmd_seq_counter;
static uint32_t r_cmd_id; /*recipe command id*/
static char gcode_buf[30];
static uint32_t r_cmd_seq[100];
static uint32_t a_axis_motor_period_counter = 0; /*This counter is used to reduce motor service routine's frequency*/
static uint32_t a_axis_motor_step_counter = 0; /*This counter is used for counting steps*/
static uint32_t a_axis_motor_direction = 0; 
static uint32_t a_axis_motor_rotation_counter = 0;

static uint32_t dispensing_motor_direction = 0;
static uint32_t dispensing_motor_rotation_counter = 0;
static uint32_t dispensing_motor_current_rotation_angle = 0;

static uint32_t servo_motor_pulse_width_counter = 0;
static uint32_t servo_motor_period_counter = 0;
static uint32_t servo_motor_state = 0;
static uint8_t bot_head_buffer = 0;
static uint32_t bothead_loadcell_average_counter = 0;
static uint32_t bothead_loadcell_sum_buffer = 0;
static uint32_t bothead_loadcell_average = 0;
static uint32_t bothead_loadcell_value = 0;
static float bothead_loadcell_weight_gram = 0;
static uint32_t bothead_loadcell_tare_flag = 0;
static float bothead_loadcell_tared = 0;
b_pot_control_info_t pot_ctrl_info[4];
static uint32_t drawer_sr_buf = 0;
static uint32_t dispensing_attempt_counter = 0;
static recipe_cmd_support_info_t * rcsi;
static float current_dispense_amount = 0;

uint32_t ejecting_dispensing_attempt_max = 0;
uint32_t rotation_dispensing_attempt_max = 0;
//static bot_head_scale_info_t bhsi;


const uint32_t a_axis_stepper_steps[8][4] = 
{
  {ON,  OFF, OFF, OFF},
  {ON,  ON,  OFF, OFF},
  {OFF, ON,  OFF, OFF},
  {OFF, ON,  ON,  OFF},
  {OFF, OFF, ON,  OFF},
  {OFF, OFF, ON,  ON},
  {OFF, OFF, OFF, ON},
  {ON,  OFF, OFF, ON}
};
const uint32_t b_axis_stepper_steps[4][4] = 
{
  {OFF, ON,  ON,  OFF},
  {OFF, ON,  OFF, ON},
  {ON, 	OFF, OFF, ON},
  {ON,  OFF, ON,  OFF}
};

const uint32_t r_cmd_matrix[20][10] = 
{
    [R_EJECTION_CARTRIDGE_DISPENSE] = 
    {
        S_PICKUP_UPRIGHT_CARTRIDGE_FROM_FRIDGE,
        S_PREPARE_FOR_DISPENSING,
        S_EJECTING_DISPENSE,
        S_RETURN_UPRIGHT_CARTRIDGE_TO_FRIDGE
//        S_ROTATION_DISPENSE
//        S_DUMPING_DISPENSE
    },
    [R_CONDIMENT_CARTRIDGE_DISPENSE] =
    {
        S_PICKUP_UPSIDE_DOWN_CARTRIDGE_FROM_FRIDGE,
        S_PREPARE_FOR_DISPENSING,
        S_ROTATION_DISPENSE,
        S_RETURN_UPSIDE_DOWN_CARTRIDGE_TO_FRIDGE
    },
    [R_ROTATIONAL_CARTRIDGE_DISPENSE] =
    {
        S_PICKUP_UPSIDE_DOWN_CARTRIDGE_FROM_FRIDGE
    }
};

const uint32_t s_cmd_matrix[S_NUM_OF_SEQ_ID_MAX][30] =
{
    [S_CALIBRATION_UPRIGHT] = 
    {
        //B_GCODE_BOT_HEAD_MOVEMENT_CHECK,
        B_OPEN_CLAMP,
        B_BOT_HEAD_TILT_RESET,
        B_GCODE_GO_TO_PARKING_PLANE,
//        B_GCODE_GO_TO_PARKING_PLANE_CENTER,
//        B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
//        B_GCODE_CABINET_ENGAGE,
//        B_GCODE_GO_TO_CABINET_OPEN_SPOT,
//        B_GCODE_GO_TO_PARKING_PLANE,
        B_GCODE_GO_TO_CABINET_ACCESS_SPOT,
        B_GCODE_GO_TO_CARTRIDGE_FRONT_SPOT,
        B_GCODE_CARTRIDGE_ENGAGE,
        B_CLOSE_CLAMP,
        B_GCODE_CARTRIDGE_LIFTUP,
        B_GCODE_GO_TO_RETRACTION_SPOT_X,
        B_GCODE_GO_TO_PARKING_PLANE,
        
        
        
//        B_A_AXIS_FRONT,
//        B_GCODE_GO_TO_CABINET_OPEN_SPOT,
//        B_GCODE_CABINET_ENGAGE,
//        B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
//        B_GCODE_GO_TO_PARKING_PLANE,
//        B_GCODE_GO_TO_PARKING_PLANE_CENTER,
//        B_RESET_A_AXIS
    },
    [S_CALIBRATION_UPSIDE_DOWN] = 
    {
        //B_GCODE_BOT_HEAD_MOVEMENT_CHECK,
        B_OPEN_CLAMP,
        B_BOT_HEAD_TILT_RESET,
        B_GCODE_GO_TO_PARKING_PLANE,
//        B_GCODE_GO_TO_PARKING_PLANE_CENTER,
//        B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
//        B_GCODE_CABINET_ENGAGE,
//        B_GCODE_GO_TO_CABINET_OPEN_SPOT,
//        B_GCODE_GO_TO_PARKING_PLANE,
        B_GCODE_GO_TO_CABINET_ACCESS_SPOT,
        B_GCODE_GO_TO_CARTRIDGE_FRONT_SPOT,
        B_BOT_HEAD_UPSIDE_DOWN,
        B_DISPENSING_MOTOR_RESET,
        B_GCODE_CARTRIDGE_ENGAGE,
        B_CLOSE_CLAMP,
//        B_GCODE_CARTRIDGE_LIFTUP,
//        B_GCODE_GO_TO_RETRACTION_SPOT_X,
//        B_GCODE_GO_TO_PARKING_PLANE,
        
        
        
//        B_A_AXIS_FRONT,
//        B_GCODE_GO_TO_CABINET_OPEN_SPOT,
//        B_GCODE_CABINET_ENGAGE,
//        B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
//        B_GCODE_GO_TO_PARKING_PLANE,
//        B_GCODE_GO_TO_PARKING_PLANE_CENTER,
//        B_RESET_A_AXIS
    },
    [S_PICKUP_UPRIGHT_CARTRIDGE_FROM_FRIDGE] = 
    {
        //B_GCODE_BOT_HEAD_MOVEMENT_CHECK,
        B_OPEN_CLAMP,
        B_BOT_HEAD_TILT_RESET,
        B_GCODE_GO_TO_PARKING_PLANE,
        B_GCODE_GO_TO_PARKING_PLANE_CENTER,
        B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
        B_GCODE_CABINET_ENGAGE,
        B_GCODE_GO_TO_CABINET_OPEN_SPOT,
        B_GCODE_GO_TO_PARKING_PLANE,
        B_GCODE_GO_TO_CABINET_ACCESS_SPOT,
        B_GCODE_GO_TO_CARTRIDGE_FRONT_SPOT,
        B_GCODE_CARTRIDGE_ENGAGE,
        B_CLOSE_CLAMP,
        B_GCODE_CARTRIDGE_LIFTUP,
        B_GCODE_GO_TO_RETRACTION_SPOT_X,
        B_GCODE_GO_TO_PARKING_PLANE,
        
        
        
//        B_A_AXIS_FRONT,
//        B_GCODE_GO_TO_CABINET_OPEN_SPOT,
//        B_GCODE_CABINET_ENGAGE,
//        B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
//        B_GCODE_GO_TO_PARKING_PLANE,
//        B_GCODE_GO_TO_PARKING_PLANE_CENTER,
//        B_RESET_A_AXIS
    },
    [S_PICKUP_UPSIDE_DOWN_CARTRIDGE_FROM_FRIDGE] = 
    {
        //B_GCODE_BOT_HEAD_MOVEMENT_CHECK,
        B_OPEN_CLAMP,
        B_BOT_HEAD_TILT_RESET,
        B_GCODE_GO_TO_PARKING_PLANE,
        B_GCODE_GO_TO_PARKING_PLANE_CENTER,
        B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
        B_GCODE_CABINET_ENGAGE,
        B_GCODE_GO_TO_CABINET_OPEN_SPOT,
        B_GCODE_GO_TO_PARKING_PLANE,
        B_GCODE_GO_TO_CABINET_ACCESS_SPOT,
        B_GCODE_GO_TO_CARTRIDGE_FRONT_SPOT,
        B_BOT_HEAD_UPSIDE_DOWN,
        B_DISPENSING_MOTOR_RESET,
        B_GCODE_CARTRIDGE_ENGAGE,
        B_CLOSE_CLAMP,
        B_GCODE_CARTRIDGE_LIFTUP,
        B_GCODE_GO_TO_RETRACTION_SPOT_X,
        B_GCODE_GO_TO_PARKING_PLANE,
        
        
        
//        B_A_AXIS_FRONT,
//        B_GCODE_GO_TO_CABINET_OPEN_SPOT,
//        B_GCODE_CABINET_ENGAGE,
//        B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
//        B_GCODE_GO_TO_PARKING_PLANE,
//        B_GCODE_GO_TO_PARKING_PLANE_CENTER,
//        B_RESET_A_AXIS
    },
    [S_RETURN_UPRIGHT_CARTRIDGE_TO_FRIDGE] = 
    {
        B_GCODE_GO_TO_PARKING_PLANE,
//        B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
//        B_GCODE_CABINET_ENGAGE,
//        B_GCODE_GO_TO_CABINET_OPEN_SPOT,
//        B_GCODE_GO_TO_PARKING_PLANE,
        B_GCODE_GO_TO_RETRACTION_SPOT_X,
        B_RESET_A_AXIS,
        B_GCODE_GO_TO_CARTRIDGE_FRONT_SPOT_LOADED,
        B_GCODE_CARTRIDGE_ENGAGE,
        B_GCODE_LOWER_CARTRIDGE_IN_PLACE,
        B_OPEN_CLAMP,
        B_GCODE_GO_TO_RETRACTION_SPOT_X,
        B_GCODE_GO_TO_PARKING_PLANE,
        B_A_AXIS_FRONT,
        B_GCODE_GO_TO_CABINET_OPEN_SPOT,
        B_GCODE_CABINET_ENGAGE,
        B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
        B_GCODE_GO_TO_PARKING_PLANE,
        B_GCODE_GO_TO_PARKING_PLANE_CENTER,
        B_RESET_A_AXIS
    },
    [S_RETURN_UPSIDE_DOWN_CARTRIDGE_TO_FRIDGE] = 
    {
        B_GCODE_GO_TO_PARKING_PLANE,
//        B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
//        B_GCODE_CABINET_ENGAGE,
//        B_GCODE_GO_TO_CABINET_OPEN_SPOT,
//        B_GCODE_GO_TO_PARKING_PLANE,
        B_GCODE_GO_TO_RETRACTION_SPOT_X,
        B_RESET_A_AXIS,
        B_GCODE_GO_TO_CARTRIDGE_FRONT_SPOT_LOADED,
        B_GCODE_CARTRIDGE_ENGAGE,
        B_GCODE_LOWER_CARTRIDGE_IN_PLACE,
        B_OPEN_CLAMP,
        B_GCODE_GO_TO_RETRACTION_SPOT_X,
        B_GCODE_GO_TO_PARKING_PLANE,
        B_A_AXIS_FRONT,
        B_GCODE_GO_TO_CABINET_OPEN_SPOT,
        B_GCODE_CABINET_ENGAGE,
        B_GCODE_GO_TO_CABINET_CLOSE_SPOT,
        B_GCODE_GO_TO_PARKING_PLANE,
        B_GCODE_GO_TO_PARKING_PLANE_CENTER,
        B_RESET_A_AXIS,
        B_BOT_HEAD_TILT_RESET,
        B_CLOSE_LID,
        B_POT_STIR_TEST
    },
    [S_PREPARE_FOR_DISPENSING] =
    {
        //B_OPEN_LID,
        B_BOT_HEAD_ORIENT,
        B_GCODE_GO_TO_POT_DISPENSING_SPOT,
        B_LOWER_TO_DISPENSE_Z
    },
    [S_EJECTING_DISPENSE] =
    {
        B_SCALE_TARE,
        B_BOT_HEAD_TILT,
        B_EJECTION_DISPENSE,
        B_SHAKE_DOWN,
        B_SHAKE_UP,
        B_SCALE_MEASURE,
        B_BOT_HEAD_TILT_RESET,
        B_DISPENSING_MOTOR_RESET
    },
    [S_ROTATION_DISPENSE] =
    {
        B_BOT_HEAD_TILT_RESET,
        B_SCALE_TARE,
        B_ROTATION_DISPENSE,
        B_SCALE_MEASURE,
        B_DISPENSING_MOTOR_RESET,
        B_BOT_HEAD_UPSIDE_DOWN,
        
    },
    [S_DUMPING_DISPENSE] =
    {
        B_SCALE_TARE,
        B_FULLY_DUMP,
        B_LIFT_UP_FOR_RE_DUMPING,
        B_FULLY_DUMP,
        B_SCALE_MEASURE,
        B_BOT_HEAD_TILT_RESET
    }
};

/*****************************************************************************
*                      Functions
*****************************************************************************/
void start_excecuting_recipe_cmd(void)
{
    r_cmd_id = recipe_cmd_id_getter();
    construct_r_cmd_seq(r_cmd_id, r_cmd_seq);
    rcsi = recipe_cmd_support_info_getter();
    send_event(ARBIT_CMD_SIG, ao_bot_controller);
}

void construct_r_cmd_seq(uint32_t current_r_cmd, uint32_t * recipe_cmd_seq)
{
    uint32_t i, j, k, curr_s_cmd;
    k = 0;
    memset(&r_cmd_seq[0], 0, sizeof(r_cmd_seq));
    for(i = 0; r_cmd_matrix[r_cmd_id][i] != S_INVALID_ID; i++)
    {
        curr_s_cmd = r_cmd_matrix[r_cmd_id][i];
        for(j = 0; s_cmd_matrix[curr_s_cmd][j] != B_INVALID_ID; j++)
        {
            recipe_cmd_seq[k] = s_cmd_matrix[curr_s_cmd][j];
            k++;
        }
    }
}

void clamp_control(uint32_t cmd)
{
    uint32_t duty_cycle;
	if(cmd == OPEN)
    {
        duty_cycle = 1950;
    }
    else
    {
        duty_cycle = 750;
    }
    __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_3, duty_cycle);
}
	
void send_gcode(void)
{
    uint32_t gcode_length;
	gcode_length = strlen(gcode_buf);
    HAL_UART_Transmit_DMA(&huart1, (uint8_t *)gcode_buf, gcode_length);
	//memset(gcode_buf,0,gcode_length);
}

void reset_gcode_buffer(void)
{
    uint32_t gcode_length;
        
    gcode_length = strlen(gcode_buf);
    memset(gcode_buf,0,gcode_length);
}

/*inside later*/

void bot_control(uint32_t cmd)
{   
    reset_gcode_buffer();
	switch(cmd)
	{
		case B_GCODE_GO_TO_PARKING_PLANE:
			sprintf(gcode_buf, "G1 Z%.2f F%d\n", rcsi->cdci.catridge_info.parking_plane_z, FEED_RATE_Z);
			send_gcode();
			break;
		case B_GCODE_GO_TO_CABINET_CLOSE_SPOT:
			sprintf(gcode_buf, "G1 X%.2f F%d\n", rcsi->cdci.catridge_info.closed_cabinet_hook_x, FEED_RATE_XY_NO_LOAD);
			send_gcode();
			break;
		case B_GCODE_CABINET_ENGAGE:
			sprintf(gcode_buf, "G1 Z%.2f F%d\n", rcsi->cdci.catridge_info.cabinet_engage_z, FEED_RATE_Z);
			send_gcode();
			break;
		case B_GCODE_GO_TO_CABINET_OPEN_SPOT:
			sprintf(gcode_buf, "G1 X%.2f F%d\n", rcsi->cdci.catridge_info.opened_cabinet_hook_x, FEED_RATE_XY_LOADED);
			send_gcode();
            break;
        case B_GCODE_GO_TO_CABINET_ACCESS_SPOT:
			sprintf(gcode_buf, "G1 X%.2f F%d\n", rcsi->cdci.catridge_info.cabinet_access_x,FEED_RATE_XY_NO_LOAD);
			send_gcode();
			break;
		case B_GCODE_GO_TO_CARTRIDGE_FRONT_SPOT:
			sprintf(gcode_buf, "G1 Y%.2f Z%.2f F%d\n", 
                    rcsi->cdci.catridge_info.cartridge_y,
                    rcsi->cdci.catridge_info.cartridge_z,
                    FEED_RATE_Z);
			send_gcode();
			break;
        case B_GCODE_GO_TO_CARTRIDGE_FRONT_SPOT_LOADED:
			sprintf(gcode_buf, "G1 Y%.2f Z%.2f F%d\n", 
                    rcsi->cdci.catridge_info.cartridge_y,
                    (rcsi->cdci.catridge_info.cartridge_z + 18),
                    FEED_RATE_Z);
			send_gcode();
			break;
		case B_GCODE_CARTRIDGE_ENGAGE:
			sprintf(gcode_buf, "G1 X%.2f F%d\n", rcsi->cdci.catridge_info.cartridge_engage_x, FEED_RATE_XY_NO_LOAD);
			send_gcode();
			break;
        case B_GCODE_CARTRIDGE_LIFTUP:
			sprintf(gcode_buf, "G1 Z%.2f F%d\n", rcsi->cdci.catridge_info.cartridge_liftup_z, FEED_RATE_Z);
			send_gcode();
			break;
		case B_GCODE_GO_TO_RETRACTION_SPOT_X:
			sprintf(gcode_buf, "G1 X%.2f F%d\n", rcsi->cdci.catridge_info.cabinet_retract_x, FEED_RATE_XY_LOADED);
			send_gcode();
			break;
        case B_GCODE_LOWER_CARTRIDGE_IN_PLACE:
            sprintf(gcode_buf, "G1 Z%.2f F%d\n", rcsi->cdci.catridge_info.cartridge_z, FEED_RATE_Z);
            send_gcode();
            break;
		case B_GCODE_GO_TO_PARKING_PLANE_CENTER:
			sprintf(gcode_buf, "G1 X%.2f Y%.2f F%d\n", 
                    rcsi->cdci.pot_info.drawer_center_x,
                    rcsi->cdci.pot_info.drawer_center_y,
                    FEED_RATE_XY_NO_LOAD);
			send_gcode();
			break;
        case B_GCODE_GO_TO_POT_DISPENSING_SPOT:
			sprintf(gcode_buf, "G1 X%.2f Y%.2f F%d\n", 
                    rcsi->cdci.pot_info.pot_dispense_x,
                    rcsi->cdci.pot_info.pot_dispense_y,
                    FEED_RATE_XY_NO_LOAD);
			send_gcode();
			break;            
            break;
		case B_GCODE_CHANGE_TO_ABSOLUTE_MODE:
			strcpy(gcode_buf, "G90");
			send_gcode();
			break;
		case B_GCODE_CHANGE_TO_RELATIVE_MODE:
			strcpy(gcode_buf, "G91");
			send_gcode();
			break;
		case B_GCODE_BOT_HEAD_MOVEMENT_CHECK:
			strcpy(gcode_buf, "?");
			send_gcode();
			break;
        case B_LOWER_TO_DISPENSE_Z:
            sprintf(gcode_buf, "G1 Z%.2f F%d\n", rcsi->cdci.pot_info.pot_dispense_z, FEED_RATE_Z);
            send_gcode();
            break;
		case B_CLOSE_CLAMP:
            clamp_control(CLOSED);
            cmd_execute_delay(1000);
			break;
		case B_OPEN_CLAMP:
            clamp_control(OPEN);
            cmd_execute_delay(1000);
			break;
        case B_FULLY_DUMP:
            b_axis_motor_control(180);
            cmd_execute_delay(2000);
            break;
        case B_BOT_HEAD_TILT_RESET:
            b_axis_motor_control(0);
            cmd_execute_delay(1000);
            break;
        case B_OPEN_LID:
            pot_lift_motor_control(rcsi->cdci.pot_info.pot_id, UP);
            cmd_execute_delay(4000);
            break;
        case B_CLOSE_LID:
            pot_lift_motor_control(rcsi->cdci.pot_info.pot_id, DOWN);
            cmd_execute_delay(4000);
            break;
        case B_POT_STIR:
            pot_stir_motor_control(rcsi->psci.pot_id, 
                                   rcsi->psci.pot_stir_direction, 
                                   rcsi->psci.pot_stir_timeout);
            break;
        case B_POT_STIR_TEST:
            pot_stir_motor_control(0, 
                                   CW, 
                                   100000);
            cmd_execute_delay(1000);
            break;
        case B_ROTATION_DISPENSE:
            dispensing_motor_control(CW, 45);
            dispensing_motor_current_rotation_angle += 45;
            if(dispensing_motor_current_rotation_angle > DISPENSE_MOTOR_ROTATION_ANGLE_LIMIT)
            {
                bot_control(B_DISPENSING_MOTOR_RESET);
            }
            else
            {
                cmd_execute_delay(500);
            }
            break;
        case B_EJECTION_DISPENSE:
            dispensing_motor_control(CW, 45);
            dispensing_motor_current_rotation_angle += 45;
            if(dispensing_motor_current_rotation_angle > DISPENSE_MOTOR_ROTATION_ANGLE_LIMIT)
            {
                bot_control(B_DISPENSING_MOTOR_RESET);
                bot_control(B_FULLY_DUMP);
                bot_control(B_BOT_HEAD_TILT_RESET);
            }
            else
            {
                cmd_execute_delay(1500);
            }
            break;
        case B_DISPENSING_MOTOR_RESET:
            dispensing_motor_control(CCW, dispensing_motor_current_rotation_angle);
            dispensing_motor_current_rotation_angle = 0;
            cmd_execute_delay(3000);
            break;
        case B_DISPENSING_MOTOR_RESET_90:
            dispensing_motor_control(CCW, 90);
            dispensing_motor_current_rotation_angle = 0;
            cmd_execute_delay(2000);
            break;
        case B_SCALE_MEASURE:
            rcsi->cdci.scale_info.scale_measure_flag = ON;
            break;
        case B_SCALE_TARE:
            rcsi->cdci.scale_info.scale_tare_flag = ON;
            break;

        case B_LIFT_UP_FOR_RE_DUMPING:
            b_axis_motor_control(120);
            cmd_execute_delay(300);
            break;
        case B_DISPENSE_SET_BACK:
            dispensing_set_back(rcsi->cdci.catridge_info.cartridge_type);
            break;
        case B_HOMING:
            strcpy(gcode_buf, "$H\n");
			send_gcode();
            break;
        case B_BOT_HEAD_TILT:
            b_axis_motor_control(45);
            cmd_execute_delay(2000);
            break;
        case B_BOT_HEAD_UPSIDE_DOWN:
            b_axis_motor_control(180);
            cmd_execute_delay(2000);
            break;
        case B_SHAKE_DOWN:
            b_axis_motor_control(60);
            cmd_execute_delay(200);
            break;
        case B_SHAKE_UP:
            b_axis_motor_control(45);
            cmd_execute_delay(1500);
            break;
        case B_RESET_A_AXIS:
            a_axis_motor_control(CW, 360);
            cmd_execute_delay(4000);
            break;
        case B_A_AXIS_FRONT:
            a_axis_motor_control(CCW, 93);
            cmd_execute_delay(2000);
            break;
        case B_A_AXIS_RIGHT:
            a_axis_motor_control(CCW, 180);
            cmd_execute_delay(2000);
            break;
        case B_BOT_HEAD_ORIENT:
            bot_head_orient(rcsi->cdci.pot_info.rotation_direction,
                            rcsi->cdci.pot_info.rotation_angle);
            cmd_execute_delay(2000);
            break;
        default:
            while(1){};
            break;
	}
}

void bot_head_orient(uint32_t direction, uint32_t angle)
{
    a_axis_motor_control(direction, angle);
}

uint32_t is_dispensing_max_attempt_reached(void)
{
    return (dispensing_attempt_counter >= DISPENSING_ATTEMPT_MAX);
}
    
void process_current_cmd(void)
{
	bot_control(r_cmd_seq[cmd_seq_counter]);
}

void timer_call_back(void)
{
    //update_drawer_shift_register();
    //bot_head_update();
    bot_head_update();
    update_drawer_shift_register();
}

void toggle_clamp(void)
{
	servo_motor_state ^= 1;
}

/* This function is used to peek next cmd type so the cmd parser can stop and wait for 
*  bot head to stop moving when the next type is not gcode cmd. If the next cmd
*  is gcode command then it's ok for command parser to continue sending gcode 
*  to grbl.
*/
uint32_t current_cmd_type(void)
{
	uint32_t cmd_type;
    if(r_cmd_seq[cmd_seq_counter] == EMPTY_CMD_TYPE)
    {
        cmd_type = EMPTY_CMD_TYPE;
    }
	else if(r_cmd_seq[cmd_seq_counter] < B_GCODE_ID_MAX)
	{
		cmd_type = GCODE_TYPE;
	}
    else if(r_cmd_seq[cmd_seq_counter] == B_SCALE_MEASURE)
    {
        cmd_type = BOT_HEAD_SCALE_MEASURE_TYPE;
    }
	else
	{
		cmd_type = STANDARD_TYPE;
	}
	return cmd_type;
}

uint32_t current_cmd_id(void)
{
    return r_cmd_seq[cmd_seq_counter];
}

void cmd_processing_done(void)
{
	cmd_seq_counter = 0;
	send_event(CMD_PROCESSING_DONE_SIG, ao_protocol_manager);
}

/* this argument can be positive or negative depend on weather need to go back
*  to re execute command. for example, dispensing action needs multiple iteration
*  of dispensing action. the counter needs to go back. however, most of case 
*  cmd_step will be just equal to 1*/
void next_cmd(int32_t cmd_step)
{
    cmd_seq_counter += cmd_step;
}

/*
*   This shift reigster update routin is called every 1ms from ao_bot_controller's
*    periotic time event
*/
extern uint32_t toggle;
uint32_t buf1; 
uint32_t stall_detected_flag;
uint32_t stall_counter;
void update_drawer_shift_register(void)
{
    /*update pot lifting motor control signal*/
    uint32_t i;
    for(i = 0; i < 4; i++)
    {
        /*motor use full power to lift the lid then use half power to stay vertical*/
        if(pot_ctrl_info[i].lift_motor_direction == UP)
        {
//            if(stall_detect())
//            {
//                stall_counter++;
//                if(stall_counter == 100);
//                {
//                    //stall_counter = 0;
//                    stall_detected_flag = 1;
//                }
//            }
//            if(!stall_detected_flag)
            if(pot_ctrl_info[i].lift_motor_moving_timeout)
            {
                /*pwm period is 5*/
                pot_ctrl_info[i].lift_motor_pwm_duty_cycle = 5;
                pot_ctrl_info[i].lift_motor_moving_timeout--;
            }
            else
            {
                pot_ctrl_info[i].lift_motor_pwm_duty_cycle = 1;
            }
            if(pot_ctrl_info[i].lift_motor_pwm_counter == pot_ctrl_info[i].lift_motor_pwm_period)
            {
                SET_BUF_BIT(drawer_sr_buf, POT_BASE(i) + LIFT_MOTOR_P);
                CLEAR_BUF_BIT(drawer_sr_buf, POT_BASE(i) + LIFT_MOTOR_N);
                pot_ctrl_info[i].lift_motor_pwm_counter = 0;
            }
            else if(pot_ctrl_info[i].lift_motor_pwm_counter >= pot_ctrl_info[i].lift_motor_pwm_duty_cycle)
            {
                CLEAR_BUF_BIT(drawer_sr_buf, POT_BASE(i) + LIFT_MOTOR_P);
            }
            pot_ctrl_info[i].lift_motor_pwm_counter++;
        }
        /*lid going down is simply using power because gravity helps it*/
        if(pot_ctrl_info[i].lift_motor_direction == DOWN)
        {
            if(pot_ctrl_info[i].lift_motor_moving_timeout)
            {
                pot_ctrl_info[i].lift_motor_pwm_duty_cycle = 2;
                pot_ctrl_info[i].lift_motor_moving_timeout--;
                if(pot_ctrl_info[i].lift_motor_pwm_counter == pot_ctrl_info[i].lift_motor_pwm_period)
                {
                    SET_BUF_BIT(drawer_sr_buf, POT_BASE(i) + LIFT_MOTOR_N);
                    CLEAR_BUF_BIT(drawer_sr_buf, POT_BASE(i) + LIFT_MOTOR_P);
                    pot_ctrl_info[i].lift_motor_pwm_counter = 0;
                }
                else if(pot_ctrl_info[i].lift_motor_pwm_counter == pot_ctrl_info[i].lift_motor_pwm_duty_cycle)
                {
                    CLEAR_BUF_BIT(drawer_sr_buf, POT_BASE(i) + LIFT_MOTOR_N);     
                }
                pot_ctrl_info[i].lift_motor_pwm_counter++;
            }
            else
            {
                pot_ctrl_info[i].lift_motor_pwm_duty_cycle = 0;
                /*testing*/
                CLEAR_BUF_BIT(drawer_sr_buf, POT_BASE(i) + LIFT_MOTOR_N);
                CLEAR_BUF_BIT(drawer_sr_buf, POT_BASE(i) + LIFT_MOTOR_P);
            }

        }
        
        /*update stir motor direction*/
        if(pot_ctrl_info[i].stir_motor_timeout || pot_ctrl_info[i].stir_motor_infinite_rotation)
        {
            if(pot_ctrl_info[i].stir_motor_direction == CW)
            {
                SET_BUF_BIT(drawer_sr_buf, POT_BASE(i) + STIR_MOTOR_P);
                CLEAR_BUF_BIT(drawer_sr_buf, POT_BASE(i) + STIR_MOTOR_N);
            }
            if(pot_ctrl_info[i].stir_motor_direction == CCW)
            {
                SET_BUF_BIT(drawer_sr_buf, POT_BASE(i) + STIR_MOTOR_N);
                CLEAR_BUF_BIT(drawer_sr_buf, POT_BASE(i) + STIR_MOTOR_P);            
            }
            pot_ctrl_info[i].stir_motor_timeout--;
        }
        else
        {
            CLEAR_BUF_BIT(drawer_sr_buf, POT_BASE(i) + STIR_MOTOR_P);
            CLEAR_BUF_BIT(drawer_sr_buf, POT_BASE(i) + STIR_MOTOR_N);
        }
    }
    
    /*Update frame shift register*/
    if(toggle)
    {
        SET_BUF_BIT(drawer_sr_buf, 8);
        SET_BUF_BIT(drawer_sr_buf, 9);
        SET_BUF_BIT(drawer_sr_buf, 10);
        SET_BUF_BIT(drawer_sr_buf, 11);
        SET_BUF_BIT(drawer_sr_buf, 12);
        SET_BUF_BIT(drawer_sr_buf, 13);
        SET_BUF_BIT(drawer_sr_buf, 14);
        SET_BUF_BIT(drawer_sr_buf, 15);
    }
    else
    {
        CLEAR_BUF_BIT(drawer_sr_buf, 8);
        CLEAR_BUF_BIT(drawer_sr_buf, 9);
        CLEAR_BUF_BIT(drawer_sr_buf, 10);
        CLEAR_BUF_BIT(drawer_sr_buf, 11);
        CLEAR_BUF_BIT(drawer_sr_buf, 12);
        CLEAR_BUF_BIT(drawer_sr_buf, 13);
        CLEAR_BUF_BIT(drawer_sr_buf, 14);
        CLEAR_BUF_BIT(drawer_sr_buf, 15);
    }
    /*update shift register using SPI*/
    if(HAL_SPI_Transmit_DMA(&drawer_module_spi_handle, (uint8_t *)&drawer_sr_buf, 4) != HAL_OK)
    {
        Error_Handler();
        
    }
} 

void pot_lift_motor_control(uint32_t pot_id, uint32_t direction)
{
    
    pot_ctrl_info[pot_id].lift_motor_direction = direction;
    if(direction == UP)
    {
        pot_ctrl_info[pot_id].lift_motor_pwm_period = 5;
        pot_ctrl_info[pot_id].lift_motor_moving_timeout = 3000;        
    }
    if(direction == DOWN)
    {
        pot_ctrl_info[pot_id].lift_motor_pwm_period = 5;
        pot_ctrl_info[pot_id].lift_motor_pwm_duty_cycle = 2;
        pot_ctrl_info[pot_id].lift_motor_moving_timeout = 2500;
    }
}

void pot_stir_motor_control(uint32_t pot_id, uint32_t direction, uint32_t timeout)
{
    pot_ctrl_info[pot_id].stir_motor_direction = direction;
    if(timeout == INFINITE)
    {
        pot_ctrl_info[pot_id].stir_motor_infinite_rotation = ON;
    }
    else
    {
        pot_ctrl_info[pot_id].stir_motor_infinite_rotation = OFF;
        pot_ctrl_info[pot_id].stir_motor_timeout = timeout;
    }
    
}

void bot_head_update(void)
{
    /************************Process A axis motor**************************/
    if(a_axis_motor_period_counter == 1) /*update period is 2ms, counting from 0*/
    {
        a_axis_motor_period_counter = 0;
        if(a_axis_motor_rotation_counter > 0)
        {
            a_axis_motor_rotation_counter--;
            bot_head_buffer &= 0x0F;
            
            bot_head_buffer |= a_axis_stepper_steps[a_axis_motor_step_counter][0] << 4;
            bot_head_buffer |= a_axis_stepper_steps[a_axis_motor_step_counter][1] << 5;
            bot_head_buffer |= a_axis_stepper_steps[a_axis_motor_step_counter][2] << 6;
            bot_head_buffer |= a_axis_stepper_steps[a_axis_motor_step_counter][3] << 7;
            if(a_axis_motor_direction == CCW)
            {
                if(a_axis_motor_step_counter == 7)
                {
                    a_axis_motor_step_counter = 0;
                }
                else
                {
                    a_axis_motor_step_counter++;
                }              
            }
            if(a_axis_motor_direction == CW)
            {
                if(a_axis_motor_step_counter == 0)
                {
                    a_axis_motor_step_counter = 7;
                }
                else
                {
                    a_axis_motor_step_counter--;
                }              
            }   
        }
    }
    else
    {
        a_axis_motor_period_counter++;
    }
    /*************************Process dispensing motor*********************/
    if(dispensing_motor_rotation_counter > 0)
    {
        dispensing_motor_rotation_counter--;
        if(dispensing_motor_direction == CW)
        {
            CLEAR_BUF_BIT(bot_head_buffer, DISPENSING_MOTOR_1_BIT);
            SET_BUF_BIT(bot_head_buffer, DISPENSING_MOTOR_2_BIT);
//            bot_head_buffer &= 0xF3;
//            bot_head_buffer |= (1 << 2);
        }
        if(dispensing_motor_direction == CCW)
        {
            CLEAR_BUF_BIT(bot_head_buffer, DISPENSING_MOTOR_2_BIT);
            SET_BUF_BIT(bot_head_buffer, DISPENSING_MOTOR_1_BIT);
        }
    }
    else
    {
        CLEAR_BUF_BIT(bot_head_buffer, DISPENSING_MOTOR_1_BIT);
        CLEAR_BUF_BIT(bot_head_buffer, DISPENSING_MOTOR_2_BIT);
    }
    /************************Process servo motor***************************/
    if(servo_motor_period_counter == 20)
    {
        servo_motor_period_counter = 0;
        servo_motor_pulse_width_counter = 0;
        SET_BUF_BIT(bot_head_buffer, CLAMP_SERVO_BIT);
    }
    else
    {
        if(servo_motor_state == OPEN)
        {
            if(servo_motor_pulse_width_counter == 1)
            {
                servo_motor_pulse_width_counter = 0;
                CLEAR_BUF_BIT(bot_head_buffer, CLAMP_SERVO_BIT);
            }
        }
        if(servo_motor_state == CLOSED)
        {
            if(servo_motor_pulse_width_counter == 2)
            {
                servo_motor_pulse_width_counter = 0;
                CLEAR_BUF_BIT(bot_head_buffer, CLAMP_SERVO_BIT);
            }
        }
    }
    servo_motor_period_counter++;
    servo_motor_pulse_width_counter++;			

    /**************************Process load cell***************************/
    if(HAL_GPIO_ReadPin(bot_head_scale.gpioData, bot_head_scale.pinData)==1)
    {
    }
    else
    {
        bothead_loadcell_value = HX711_Value(bot_head_scale);
        bothead_loadcell_sum_buffer += bothead_loadcell_value;
        bothead_loadcell_average_counter++;
        if(bothead_loadcell_average_counter == 10)
        {
            bothead_loadcell_average_counter = 0;
            bothead_loadcell_average = bothead_loadcell_sum_buffer / 10 / bot_head_scale.scale_divider;
            rcsi->cdci.scale_info.current_raw_weight = bothead_loadcell_average;
            bothead_loadcell_sum_buffer = 0;
            if(rcsi->cdci.scale_info.scale_tare_flag == ON)
            {
                rcsi->cdci.scale_info.scale_tare_flag = OFF;
                rcsi->cdci.scale_info.bare_weight_before_dispensing = rcsi->cdci.scale_info.current_raw_weight 
                                                   - BOT_HEAD_WEIGHT
                                                   - rcsi->cdci.scale_info.cartridge_weight;
                send_event(CMD_EXECUTION_DONE_SIG, ao_bot_controller);
            }
            if(rcsi->cdci.scale_info.scale_measure_flag == ON)
            {
                rcsi->cdci.scale_info.scale_measurement_counter++;
                rcsi->cdci.scale_info.scale_measure_flag = OFF;
                rcsi->cdci.scale_info.current_bare_weight = rcsi->cdci.scale_info.current_raw_weight
                                          - BOT_HEAD_WEIGHT
                                          - rcsi->cdci.scale_info.cartridge_weight;
                rcsi->cdci.scale_info.weight_dispensed = rcsi->cdci.scale_info.bare_weight_before_dispensing
                                             - rcsi->cdci.scale_info.current_bare_weight;
                if(rcsi->cdci.scale_info.weight_dispensed >= rcsi->cdci.scale_info.weight_to_dispense)
                {
                    send_event(DISPENSING_COMPLETE_SIG, ao_bot_controller);
                }
                if(rcsi->cdci.recipe_cmd_id == 1)
                {
                    //if(rcsi->cdci.scale_info.weight_dispensed < rcsi->cdci.scale_info.weight_to_dispense)
                    if(rcsi->cdci.scale_info.scale_measurement_counter <= 5)
                    {
                        send_event(DISPENSING_SET_BACK_SIG, ao_bot_controller);
                    }
                    if(rcsi->cdci.scale_info.scale_measurement_counter > 5)//DISPENSING_ATTEMPT_MAX)
                    {
                        rcsi->cdci.scale_info.scale_measurement_counter = 0;
                        send_event(DISPENSING_COMPLETE_SIG, ao_bot_controller);
                        
                        //send_event(DISPENSING_MAX_ATTEMPT_REACHED_SIG, ao_bot_controller);
                    }
                }
                else
                {
                    //if(rcsi->cdci.scale_info.weight_dispensed < rcsi->cdci.scale_info.weight_to_dispense)
                    if(rcsi->cdci.scale_info.scale_measurement_counter <= 30)
                    {
                        send_event(DISPENSING_SET_BACK_SIG, ao_bot_controller);
                    }
                    if(rcsi->cdci.scale_info.scale_measurement_counter > 30)//DISPENSING_ATTEMPT_MAX)
                    {
                        rcsi->cdci.scale_info.scale_measurement_counter = 0;
                        send_event(DISPENSING_COMPLETE_SIG, ao_bot_controller);
                        
                        //send_event(DISPENSING_MAX_ATTEMPT_REACHED_SIG, ao_bot_controller);
                    }
                }

            }
        }
    }
    /*Update shift register using SPI*/
    if(HAL_SPI_Transmit_DMA(&bot_head_spi_handle, (uint8_t *)&bot_head_buffer, 1) != HAL_OK)
    {
        Error_Handler();
    }
}

void a_axis_motor_control(uint32_t direction, uint32_t angle)
{
    a_axis_motor_direction = direction;
    a_axis_motor_rotation_counter = (uint32_t)((float)angle / 360 * 96 * 42.5 * 44 / 24);
    //a_axis_motor_rotation_counter = (uint32_t)((float)angle / 360 * 96 * 42.5);
}



void b_axis_motor_control(uint32_t angle)
{
    uint32_t duty_cycle;
    duty_cycle =  2260 - (uint32_t)(((float) angle / 180) * (2260-705));
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, duty_cycle);
}

void dispensing_motor_control(uint32_t direction, uint32_t angle)
{
    dispensing_motor_direction = direction;
    dispensing_motor_rotation_counter = (uint32_t)((float) angle / 360 * 2700);
}

void tare_load_cell(void)
{
    bothead_loadcell_tare_flag = ON;
}

void dispensing_set_back(uint32_t cartridge_type)
{
    switch(cartridge_type)
    {
        case EJECTION_TYPE:
            next_cmd(-3);
            break;
        case CONDIMENT_TYPE:
        case ROTATIONAL_TYPE:
            next_cmd(-1);
            break;
        case DUMPING_TYPE:
            next_cmd(-2);
            break;
    }
}
