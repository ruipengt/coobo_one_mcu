#ifndef gpio_h
#define gpio_h

#include "stm32f4xx_hal.h"

/****************************************************************************
*                      macros
*****************************************************************************/
#define PORTA 0
#define PORTB 1
#define PORTC 2
#define PORTD 3
#define PORTE 4
#define PORTF 5
#define PORTG 6
#define PORTH 7
#define PORTI 8

/****************************************************************************
*                      macros
*****************************************************************************/
#define GPIO_NUM(port, pin) ((port << 4) | (pin))
#define GPIO_PORT(n) (n >> 4)
#define GPIO_PIN(n) (n & 0xf)
#define GPIO_BIT(n) (1 << GPIO_PIN(n))
#define GPIO_BASE(n) ((GPIO_TypeDef  *)(GPIOA_BASE + (GPIO_PORT(n) * 0x400)))

/****************************************************************************
*                      pin definitions
*****************************************************************************/

/*gpio output*/
#define LED_1           GPIO_NUM(PORTD, 12)
#define LED_2           GPIO_NUM(PORTD, 13)
#define LED_3           GPIO_NUM(PORTD, 14)
#define LED_4           GPIO_NUM(PORTD, 15)
#define STEPPER_A_1     GPIO_NUM(PORTC, 0)
#define STEPPER_A_2     GPIO_NUM(PORTC, 1)
#define STEPPER_A_3     GPIO_NUM(PORTC, 2)
#define STEPPER_A_4     GPIO_NUM(PORTC, 3)
#define BUTTON          GPIO_NUM(PORTA, 0)
#define BOT_HEAD_SR_LATCH       GPIO_NUM(PORTB, 8)
#define DRAWER_SR_LATCH         GPIO_NUM(PORTE, 3)
#define GPIO_TEST       GPIO_NUM(PORTD, 14)

/*gpio input*/
#define WAKEUP_BUTTON   GPIO_NUM(PORTA, 0)


/****************************************************************************
*                       function prototypes
*****************************************************************************/
void gpio_init(void);
void interrupt_gpio_init(void);

#endif