//-----------------------------------------------------------------------------

#include "stm32f4xx_hal.h"
#include "gpio.h"
#include "global.h"

//-----------------------------------------------------------------------------

typedef struct gpio_info {
    uint32_t num;       // gpio number - as defined in gpio.h
    uint32_t mode;      // input, output, etc.
    uint32_t pull;      // pull up/down, etc.
    uint32_t speed;     // slew rate
    uint32_t alt;       // alternate pin functions
    int init;           // initial pin value
} GPIO_INFO;

/****************************************************************************
*                       gpio init config
*****************************************************************************/

static const GPIO_INFO gpio_info[] = {
    {LED_1, GPIO_MODE_OUTPUT_PP, GPIO_PULLUP, GPIO_SPEED_FREQ_HIGH, 0, GPIO_PIN_RESET},
    {LED_2, GPIO_MODE_OUTPUT_PP, GPIO_PULLUP, GPIO_SPEED_FREQ_HIGH, 0, GPIO_PIN_RESET},
    {LED_3, GPIO_MODE_OUTPUT_PP, GPIO_PULLUP, GPIO_SPEED_FREQ_HIGH, 0, GPIO_PIN_RESET},
    {LED_4, GPIO_MODE_OUTPUT_PP, GPIO_PULLUP, GPIO_SPEED_FREQ_HIGH, 0, GPIO_PIN_RESET},
    {STEPPER_A_1, GPIO_MODE_OUTPUT_PP, GPIO_PULLDOWN, GPIO_SPEED_FREQ_HIGH, 0, GPIO_PIN_RESET},
    {STEPPER_A_2, GPIO_MODE_OUTPUT_PP, GPIO_PULLDOWN, GPIO_SPEED_FREQ_HIGH, 0, GPIO_PIN_RESET},
    {STEPPER_A_3, GPIO_MODE_OUTPUT_PP, GPIO_PULLDOWN, GPIO_SPEED_FREQ_HIGH, 0, GPIO_PIN_RESET},
    {STEPPER_A_4, GPIO_MODE_OUTPUT_PP, GPIO_PULLDOWN, GPIO_SPEED_FREQ_HIGH, 0, GPIO_PIN_RESET},
    {BOT_HEAD_SR_LATCH, GPIO_MODE_OUTPUT_PP, GPIO_PULLDOWN, GPIO_SPEED_FREQ_HIGH, 0, GPIO_PIN_RESET},
    {DRAWER_SR_LATCH, GPIO_MODE_OUTPUT_PP, GPIO_PULLDOWN, GPIO_SPEED_FREQ_HIGH, 0, GPIO_PIN_RESET},
    {GPIO_TEST, GPIO_MODE_OUTPUT_PP, GPIO_PULLDOWN, GPIO_SPEED_FREQ_HIGH, 0, GPIO_PIN_RESET}
    // push buttons
    //{BUTTON, GPIO_MODE_IT_FALLING, GPIO_NOPULL, 0, 0, 5},
    //{GPIO_NUM(PORTA, 5), GPIO_MODE_IT_FALLING, GPIO_NOPULL, 0, 0, -1},
    // g540 keepalive (tim4 function)
    //{GPIO_NUM(PORTA, 6), GPIO_MODE_AF_PP, GPIO_NOPULL, GPIO_SPEED_FREQ_HIGH, GPIO_AF2_TIM4, -1},
    // serial port (usart2 function)
    //{GPIO_NUM(PORTA, 7), GPIO_MODE_AF_PP, GPIO_NOPULL, GPIO_SPEED_FREQ_HIGH, GPIO_AF7_USART2, -1},
    //{GPIO_NUM(PORTA, 8), GPIO_MODE_AF_PP, GPIO_NOPULL, GPIO_SPEED_FREQ_HIGH, GPIO_AF7_USART2, -1}
};



void gpio_init(void)
{
    uint32_t i;
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOE_CLK_ENABLE();

    for (i = 0; i < sizeof(gpio_info)/sizeof(GPIO_INFO); i++) {
        const GPIO_INFO *gpio = &gpio_info[i];
        GPIO_InitTypeDef GPIO_InitStruct;
        // enable the peripheral clock: __GPIOx_CLK_ENABLE()
        RCC->AHB1ENR |= (1 << GPIO_PORT(gpio->num));
        // setup the gpio port/pin
        GPIO_InitStruct.Pin = GPIO_BIT(gpio->num);
        GPIO_InitStruct.Mode = gpio->mode;
        GPIO_InitStruct.Pull = gpio->pull;
        GPIO_InitStruct.Speed = gpio->speed;
        GPIO_InitStruct.Alternate = gpio->alt;
        HAL_GPIO_Init(GPIO_BASE(gpio->num), &GPIO_InitStruct);
        // set any initial value
        if (gpio->init >= 0) {
            HAL_GPIO_WritePin(GPIO_BASE(gpio->num), GPIO_BIT(gpio->num), gpio->init);
        }
    }
}

void interrupt_gpio_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    __HAL_RCC_GPIOD_CLK_ENABLE();
      /*Configure GPIO pin : PA0 */
    GPIO_InitStruct.Pin = GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
    
    HAL_NVIC_SetPriority(EXTI3_IRQn, DRAWER_PRIO, 0);
    HAL_NVIC_EnableIRQ(EXTI3_IRQn);
}