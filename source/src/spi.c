/* Includes ------------------------------------------------------------------*/
#include "spi.h"
#include "pin_def.h"
#include "global.h"


/* Private variables ---------------------------------------------------------*/
/* SPI handler declaration */
SPI_HandleTypeDef bot_head_spi_handle;
SPI_HandleTypeDef drawer_module_spi_handle;

DMA_HandleTypeDef hdma_spi1_tx;
DMA_HandleTypeDef hdma_spi4_tx;
/****************************************************************************
*     For shift register, the least significant bit correspond to Port A
*****************************************************************************/

void bot_head_spi_init(void)
{
      /*##-1- Configure the SPI peripheral #######################################*/
    /* Set the SPI parameters */
    bot_head_spi_handle.Instance               = BOTHEAD_SPI;
    bot_head_spi_handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
    bot_head_spi_handle.Init.Direction         = SPI_DIRECTION_2LINES;
    bot_head_spi_handle.Init.CLKPhase          = SPI_PHASE_1EDGE;
    bot_head_spi_handle.Init.CLKPolarity       = SPI_POLARITY_HIGH;
    bot_head_spi_handle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
    bot_head_spi_handle.Init.CRCPolynomial     = 7;
    bot_head_spi_handle.Init.DataSize          = SPI_DATASIZE_8BIT;
    bot_head_spi_handle.Init.FirstBit          = SPI_FIRSTBIT_LSB;
    bot_head_spi_handle.Init.NSS               = SPI_NSS_SOFT;
    bot_head_spi_handle.Init.TIMode            = SPI_TIMODE_DISABLE;

    bot_head_spi_handle.Init.Mode = SPI_MODE_MASTER;


    if(HAL_SPI_Init(&bot_head_spi_handle) != HAL_OK)
    {
    /* Initialization Error */
    Error_Handler();
    }
}

void drawer_module_spi_init(void)
{
    drawer_module_spi_handle.Instance = SPI4;
    drawer_module_spi_handle.Init.Mode = SPI_MODE_MASTER;
    drawer_module_spi_handle.Init.Direction = SPI_DIRECTION_2LINES;
    drawer_module_spi_handle.Init.DataSize = SPI_DATASIZE_8BIT;
    drawer_module_spi_handle.Init.CLKPolarity = SPI_POLARITY_HIGH;
    drawer_module_spi_handle.Init.CLKPhase = SPI_PHASE_1EDGE;
    drawer_module_spi_handle.Init.NSS = SPI_NSS_SOFT;
    drawer_module_spi_handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
    drawer_module_spi_handle.Init.FirstBit = SPI_FIRSTBIT_LSB;
    drawer_module_spi_handle.Init.TIMode = SPI_TIMODE_DISABLE;
    drawer_module_spi_handle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    drawer_module_spi_handle.Init.CRCPolynomial = 10;
    if (HAL_SPI_Init(&drawer_module_spi_handle) != HAL_OK)
    {
    Error_Handler();
    }    
}

void HAL_SPI_MspInit(SPI_HandleTypeDef *spiHandle)
{
    GPIO_InitTypeDef  GPIO_InitStruct;
    if(spiHandle->Instance==SPI1)
    {
        /*##-1- Enable peripherals and GPIO Clocks #################################*/
        /* Enable GPIO TX/RX clock */
        BOTHEAD_SPI_SCK_GPIO_CLK_ENABLE();
        BOTHEAD_SPI_MISO_GPIO_CLK_ENABLE();
        BOTHEAD_SPI_MOSI_GPIO_CLK_ENABLE();
        /* Enable SPI3 clock */
        BOTHEAD_SPI_CLK_ENABLE(); 
        /* Enable DMA1 clock */
        DMAx_CLK_ENABLE();   

        /*##-2- Configure peripheral GPIO ##########################################*/  
        /* SPI SCK GPIO pin configuration  */
        GPIO_InitStruct.Pin       = BOTHEAD_SPI_SCK_PIN|BOTHEAD_SPI_MOSI_PIN;;
        GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull      = GPIO_PULLUP;
        GPIO_InitStruct.Speed     = GPIO_SPEED_LOW;
        GPIO_InitStruct.Alternate = BOTHEAD_SPI_AF;


        HAL_GPIO_Init(BOTHEAD_SPI_PORT, &GPIO_InitStruct);

        /*##-3- Configure the DMA streams ##########################################*/
        /* Configure the DMA handler for Transmission process */
        hdma_spi1_tx.Instance                 = BOTHEAD_SPI_TX_DMA_STREAM;

        hdma_spi1_tx.Init.Channel             = BOTHEAD_SPI_TX_DMA_CHANNEL;
        hdma_spi1_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
        hdma_spi1_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
        hdma_spi1_tx.Init.MemInc              = DMA_MINC_ENABLE;
        hdma_spi1_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_spi1_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
        hdma_spi1_tx.Init.Mode                = DMA_NORMAL;
        hdma_spi1_tx.Init.Priority            = DMA_PRIORITY_LOW;
        hdma_spi1_tx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;         
        hdma_spi1_tx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
        hdma_spi1_tx.Init.MemBurst            = DMA_MBURST_INC4;
        hdma_spi1_tx.Init.PeriphBurst         = DMA_PBURST_INC4;

        HAL_DMA_Init(&hdma_spi1_tx);   

        /* Associate the initialized DMA handle to the the SPI handle */
        __HAL_LINKDMA(spiHandle, hdmatx, hdma_spi1_tx);

        /*##-4- Configure the NVIC for DMA #########################################*/ 
        /* NVIC configuration for DMA transfer complete interrupt (SPI3_TX) */
        HAL_NVIC_SetPriority(BOTHEAD_SPI_DMA_TX_IRQn, BOTHEAD_DMA_PRIO, 0);
        HAL_NVIC_EnableIRQ(BOTHEAD_SPI_DMA_TX_IRQn);


        /*##-5- Configure the NVIC for SPI #########################################*/
        HAL_NVIC_SetPriority(BOTHEAD_SPI_IRQn, BOTHEAD_SPI_PRIO, 0);
        HAL_NVIC_EnableIRQ(BOTHEAD_SPI_IRQn);
    }
  else if(spiHandle->Instance==SPI4)
  {

        /* Peripheral clock enable */
        __HAL_RCC_SPI4_CLK_ENABLE();
      
        /**SPI4 GPIO Configuration    
        PE2     ------> SPI4_SCK
        PE6     ------> SPI4_MOSI 
        */
        GPIO_InitStruct.Pin = DMOD_SHIFT_REGISTER_CLK_Pin|DMOD_SHIFT_REGISTER_DATA_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF5_SPI4;
        HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

        /* Peripheral DMA init*/
      
        hdma_spi4_tx.Instance = DMA2_Stream4;
        hdma_spi4_tx.Init.Channel = DMA_CHANNEL_5;
        hdma_spi4_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
        hdma_spi4_tx.Init.PeriphInc = DMA_PINC_DISABLE;
        hdma_spi4_tx.Init.MemInc = DMA_MINC_ENABLE;
        hdma_spi4_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_spi4_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
        hdma_spi4_tx.Init.Mode = DMA_NORMAL;
        hdma_spi4_tx.Init.Priority = DMA_PRIORITY_LOW;
        hdma_spi4_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
        if (HAL_DMA_Init(&hdma_spi4_tx) != HAL_OK)
        {
          Error_Handler();
        }

        __HAL_LINKDMA(spiHandle,hdmatx,hdma_spi4_tx);

        /* NVIC configuration for DMA transfer complete interrupt (SPI3_TX) */
        HAL_NVIC_SetPriority(DMA2_Stream4_IRQn, DRAWER_MODULE_DMA_PRIO, 0);
        HAL_NVIC_EnableIRQ(DMA2_Stream4_IRQn);

        /* Peripheral interrupt init */
        HAL_NVIC_SetPriority(SPI4_IRQn, DRAWER_MODULE_SPI_PRIO, 0);
        HAL_NVIC_EnableIRQ(SPI4_IRQn);
  }
}

/**
  * @brief SPI MSP De-Initialization 
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO, DMA and NVIC configuration to their default state
  * @param hspi: SPI handle pointer
  * @retval None
  */
void HAL_SPI_MspDeInit(SPI_HandleTypeDef *spiHandle)
{
    if(spiHandle->Instance==SPI1)
    {
        /*##-1- Reset peripherals ##################################################*/
        BOTHEAD_SPI_FORCE_RESET();
        BOTHEAD_SPI_RELEASE_RESET();

        /*##-2- Disable peripherals and GPIO Clocks ################################*/
        /* Configure SPI SCK as alternate function  */
        HAL_GPIO_DeInit(BOTHEAD_SPI_PORT, BOTHEAD_SPI_SCK_PIN);

        /* Configure SPI MOSI as alternate function  */
        HAL_GPIO_DeInit(BOTHEAD_SPI_PORT, BOTHEAD_SPI_MOSI_PIN);

        /*##-3- Disable the DMA Streams ############################################*/
        /* De-Initialize the DMA Stream associate to transmission process */
        HAL_DMA_DeInit(&hdma_spi1_tx); 


        /*##-4- Disable the NVIC for DMA ###########################################*/
        HAL_NVIC_DisableIRQ(BOTHEAD_SPI_DMA_TX_IRQn);


        /*##-5- Disable the NVIC for SPI ###########################################*/
        HAL_NVIC_DisableIRQ(BOTHEAD_SPI_IRQn);
    }
    else if(spiHandle->Instance==SPI4)
    {
    /* Peripheral clock disable */
    __HAL_RCC_SPI4_CLK_DISABLE();

    /**SPI4 GPIO Configuration    
    PE2     ------> SPI4_SCK
    PE6     ------> SPI4_MOSI 
    */
    HAL_GPIO_DeInit(GPIOE, DMOD_SHIFT_REGISTER_CLK_Pin|DMOD_SHIFT_REGISTER_DATA_Pin);

    /* Peripheral DMA DeInit*/
    HAL_DMA_DeInit(spiHandle->hdmatx);

    /* Peripheral interrupt Deinit*/
    HAL_NVIC_DisableIRQ(SPI4_IRQn);

    }
}