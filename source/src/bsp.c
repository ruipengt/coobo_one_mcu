#include "global.h"

//typedef struct
//{
//    char status[50];
//    float x;
//    float y;
//    float z;
//} g_info_t; 
//
//g_info_t gInfo; 


static char grbl_rx_buf[2];
static char grbl_line_buf[100];
static uint32_t grbl_rx_buf_counter = 0;

char tablet_rx_buf[2] = {0};
char tablet_rx_line_buf[100];
char test1[5] = "$H\n";
char test2[5] = "?";
uint32_t tablet_rx_buf_counter = 0;
char reset_buf = 0x18;
uint32_t stall_detector_buf = 0;

/****************************************************************************
*                       Function definitions
*****************************************************************************/

void QV_onIdle(void)
{
    QF_INT_ENABLE();
}

void Q_onAssert(char_t const * const module, int_t location)
{
}

void QF_onStartup(void)
{
}

void bsp_init(void)
{

}

void send_event(uint8_t sig, QActive * target)
{
    QEvt * event = Q_NEW(QEvt, sig);
    QACTIVE_POST(target, (QEvt *)event, (void *)0) ;
}

void write_pin(uint32_t pin, uint32_t state)
{
    HAL_GPIO_WritePin(GPIO_BASE(pin), GPIO_BIT(pin), (GPIO_PinState) state);
}

uint32_t read_pin(uint32_t pin)
{
    uint32_t state;
    state = HAL_GPIO_ReadPin(GPIO_BASE(pin), GPIO_BIT(pin));
    return state;
}

/****************************************************************************
*                       Interrupt Callbacks
*****************************************************************************/
uint32_t toggle;
static uint32_t servo_angle = 2275;
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if(GPIO_Pin == GPIO_PIN_3)
    {
//        //send_gcode();
//        tare_load_cell();
//        //start_cmd_seq();
//        
//        if(servo_angle == 740)
//        {
//            servo_angle = 2275;
//        }
//        else
//        {
//            servo_angle = 740;
//        }
////    //  servo_angle++;
////        set_bot_head_servo(servo_angle);
         toggle ^= 1;
         if(toggle)
         {
             write_pin(GPIO_TEST, ON);
             pot_lift_motor_control(TOP_RIGHT_POT, UP);
             //pot_stir_motor_control(TOP_RIGHT_POT, CW, 5000);
         }
         else
         {
             write_pin(GPIO_TEST, OFF);
             pot_lift_motor_control(TOP_RIGHT_POT, DOWN);
             //pot_stir_motor_control(TOP_RIGHT_POT, CCW, 0);
         }
            
////        a_axis_motor_control(toggle + 1, 360);
////        b_axis_motor_control(toggle + 1, 360);
////        dispensing_motor_control(CW, 360);
        toggle_clamp();
        send_event(RECIPE_CMD_EXCUTE_START_SIG, ao_protocol_manager);
        
    }
}

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi)
{
    if(hspi->Instance == SPI1)
    {
        write_pin(BOT_HEAD_SR_LATCH, ON);
        write_pin(BOT_HEAD_SR_LATCH, OFF);
    }
    else if(hspi->Instance == SPI4)
    {
        write_pin(DRAWER_SR_LATCH, ON);
        write_pin(DRAWER_SR_LATCH, OFF);
    }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if(htim->Instance == TIM2)
    {
        timer_call_back();
    }
}

void grbl_response_decode(char * grbl_line_buf)
{
    if(grbl_line_buf[0] == '<')
    {
        //sscanf(grbl_line_buf, "<%s|MPos:%.2f,%.2f,%.2f|FS:%d,%d>\r\n",
         //     &gInfo.status[0],&gInfo.x,&gInfo.y,&gInfo.z,&temp1,&temp2);
        if(grbl_line_buf[1] == 'I' && grbl_line_buf[2] == 'd')
        {
            send_event(GRBL_IDLE_SIG, ao_bot_controller); 
        }
        if(grbl_line_buf[1] == 'R' && grbl_line_buf[2] == 'u')
        {
            send_event(GRBL_RUNNING_SIG, ao_bot_controller); 
        }
    }
    if(grbl_line_buf[0] == 'o' && grbl_line_buf[1] == 'k')
    {
        send_event(CMD_EXECUTION_DONE_SIG, ao_bot_controller);
        
    }
}

void tablet_cmd_decode(char * cmd_buf, uint32_t buf_len)
{
    switch(cmd_buf[0])
    {
        case '$':
            HAL_UART_Transmit_DMA(&huart1, (uint8_t *)cmd_buf, buf_len);
            break;
        case '0':
            send_event(RECIPE_CMD_EXCUTE_START_SIG, ao_protocol_manager);
            break;
        case '1':
            bot_control(B_OPEN_CLAMP);
            break;
        case '2':
            bot_control(B_CLOSE_CLAMP);
            break;
        case '3':
            bot_control(B_BOT_HEAD_UPSIDE_DOWN);
            break;
        case '4':
            //bot_control(B_BOT_HEAD_TILT_RESET);
            bot_control(B_POT_STIR_TEST);
            break;
        case '5':
            bot_control(B_GCODE_GO_TO_CARTRIDGE_FRONT_SPOT);
            break;
        case '6':
            bot_control(B_RESET_A_AXIS);
            break;
        case '7':
            pot_lift_motor_control(TOP_LEFT_POT, UP);
            //bot_control(B_OPEN_LID);
            break;
        case '8':
            pot_lift_motor_control(TOP_LEFT_POT, DOWN);
            //bot_control(B_ROTATION_DISPENSE);
            //bot_control(B_CLOSE_LID);
            break;
        case '9':
            bot_control(B_DISPENSING_MOTOR_RESET_90);
            break;
        case 'h':
            //bot_control(B_HOMING);
            reset_cmd_info();
            break;
        case 's':
            bot_control(B_POT_STIR_TEST);
            //reset_cmd_info();
            break;
            
    }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *usart_handle)
{
    if(usart_handle->Instance == USART1)
    {
        strcpy(&grbl_line_buf[grbl_rx_buf_counter], &grbl_rx_buf[0]);
        grbl_rx_buf_counter++;
        if(grbl_rx_buf[0] == '\n')
        {
            grbl_response_decode(grbl_line_buf);
            //memset(grbl_line_buf, 0, grbl_rx_buf_counter);
            grbl_rx_buf_counter = 0;
        }
    }
    else if(usart_handle->Instance == USART6)
    {
        if(tablet_rx_buf[0] == '?')
        {
            HAL_UART_Transmit_DMA(&huart1, (uint8_t *)&tablet_rx_buf[0], 1);
        }
        else
        {
            HAL_UART_Transmit_DMA(&huart1, (uint8_t *)&tablet_rx_buf[0], 1);
            strcpy(&tablet_rx_line_buf[tablet_rx_buf_counter], &tablet_rx_buf[0]);
            tablet_rx_buf_counter++;
            
            if(tablet_rx_buf[0] == '\n')
            {
                tablet_cmd_decode(tablet_rx_line_buf, tablet_rx_buf_counter);
                //memset(tablet_rx_line_buf, 0, tablet_rx_buf_counter);
                tablet_rx_buf_counter = 0;
            }
        }
    }
}


void grbl_usart_start(void)
{
    if(HAL_UART_Receive_DMA(&huart1, (uint8_t *)grbl_rx_buf, 1) != HAL_OK)
    {
        Error_Handler();
    }
}

void tablet_usart_start(void)
{
    if(HAL_UART_Receive_DMA(&huart6, (uint8_t *)tablet_rx_buf, 1) != HAL_OK)
    {
        Error_Handler();
    } 
    //HAL_UART_Receive_IT(&huart6, (uint8_t *)tablet_rx_buf, 1);
}

void drawer_lift_motor_stall_detect_adc_start(void)
{
  if(HAL_ADC_Start_DMA(&hadc1, (uint32_t*)&stall_detector_buf, 1) != HAL_OK)
  {
    /* Start Conversation Error */
    Error_Handler(); 
  }
}


uint32_t stall_detect(void)
{
    if( stall_detector_buf < 4000U)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}