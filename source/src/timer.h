#ifndef TIMER_H
#define TIMER_H

extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
void MX_TIM4_Init(void);
void MX_TIM2_Init(void);
void MX_TIM3_Init(void);
void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
void motor_control_pwm_start(void);
void base_timer_start(void);
void bot_head_b_axis_servo_pwm_start(void);
void bot_head_clamp_servo_pwm_start(void);

#endif